/* variables */
var preciopresentacion = 0;
var index = 1;
var producto_detail = [];

/* Elementos */
const input_producto = document.getElementById('buscar-producto');
const btn_pagar = document.getElementById('btn-pagar');

/* eventos */

document.addEventListener('DOMContentLoaded', (e) => {
  let productos = document.querySelectorAll('#tab_productos .media');
  productos.forEach((elemento) => {
    let id_producto = elemento.getAttribute('id-producto');
    elemento.addEventListener('click', () => {
      detalle_producto(id_producto);
    });
  });
});

input_producto.addEventListener('change', () => {
  let valor_buscar = input_producto.value;
  if (valor_buscar != ''){
    buscar_producto(valor_buscar);
  }
});

$('#presentacion_select').change(function () {
  const presentacion_pk = $(this).val();
  // let url = "{% url 'update-presentacion-ajax' %}";
  let url = Urls.update_presentacion_ajax();

  $.get(url, {
    'id_presentacion': presentacion_pk,
    'accion': false
  }, function (data, status) {
    if (status == "success") {
      preciopresentacion = data['precio'];
      var cantidad = $("#cantidad_select").val();
      var subtotal = preciopresentacion * cantidad;
      document.getElementById("modal_producto_precio").innerHTML = "Sub-total $ " + subtotal;
    } else {
      alert("Error del servidor consulte al administrador");
    }
  });

});

$('#cantidad_select').change(function () {
  var cantidad = $(this).val();
  if (preciopresentacion > 0) {
    var cantidad = $("#cantidad_select").val();
    var subtotal = preciopresentacion * cantidad;
    document.getElementById("modal_producto_precio").innerHTML = "Sub-total $ " + subtotal;

  }

});

/* eventos */
btn_pagar.addEventListener('click', () => {
  // alert("se Se paga el pedido");
  var id_pedido = $("#id_pedido").val();
  // alert("se Se paga el pedido" + id_pedido);
  if (id_pedido != '') {
    let form_data = new FormData(document.getElementById('pedido_form'));
    form_data.append('estado_pedido', 'finalizado');
    url = Urls.pedido_crear();
    fetch(url, {
      method: 'post',
      body: form_data,
    })
      .then((response) => {
        console.log(response);
        return response.json();
      })
      .then((rta_server) => { //SI FUE EXITOSO LO LIMPIO Y SETE EL PK PEDIDO POR SI HACEN OTRO
        alert("Pago procesado correctamente: " + rta_server['informacion']);
        console.log(rta_server['pedido_pk']);
        $("#id_pedido").val('');
        $("#tbody_pedido").empty();
        document.getElementById("pedido_subtotal").innerHTML = "$ ";
        document.getElementById("pedido_total").innerHTML = "$ ";

      })
  } else {
    alert("Debe crear el Contacto antes de cancelar el Pedido");
  }


});



/* funciones */

function ver_productos(e) {
  let id_categoria = e.getAttribute('id-categoria');

  let string_url = window.location.origin + Urls.categoria_productos();
  let url = new URL(string_url),
    params = {'id_categoria': id_categoria};

  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))

  fetch(url, {
    method: 'get',
  })
  .then((response) => {
    return response.json();
  })
  .then((productos_list) => {

    productos_container = document.getElementById('tab_productos');
    productos_container.innerHTML = '';

    if(productos_list.length > 0) {
      productos_list.forEach((producto) => {
        console.log('producto: ', producto);
        let div_producto = crear_html_producto(producto);
        console.log(div_producto);
        productos_container.appendChild(div_producto);
        console.log(productos_container);
      });
    }else{
      productos_container.innerHTML = `
        <p>0 Registros encontrados!</p>
      `;
    }

    input_producto.value = '';
    document.getElementById('productos-tab-button').click();

  })
  .catch((error) => {
    alert("ERROR: ", error);
  });
}

function buscar_producto(valor_buscar) {
  let string_url = window.location.origin + Urls.producto_buscar();
  let url = new URL(string_url),
    params = {'buscar': valor_buscar};

  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  fetch(url, {
    method: 'get',
  })
  .then((response) => {
    return response.json();
  })
  .then((productos_list) => {
    productos_container = document.getElementById('tab_productos');
    productos_container.innerHTML = '';

    if(productos_list.length > 0) {
      productos_list.forEach((producto) => {
        let div_producto = crear_html_producto(producto)
        productos_container.appendChild(div_producto);
      });
    }else{
      productos_container.innerHTML = `
        <p>0 Registros encontrados!</p>
      `;
    }
    document.getElementById('productos-tab-button').click();
  })
  .catch((error) => {
    console.log('ERROR, ', error);
  });
}

function detalle_producto(id_producto) {
  let string_url = window.location.origin + Urls.producto_detail();
  let url = new URL(string_url),
    params = {'id_producto': id_producto};

  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  fetch(url, {
    method: 'get',
  })
  .then((response) => {
    return response.json();
  })
  .then((datos) => {
    console.log("armando producto detail", datos);
    producto_detail = datos['producto_detail'];

    document.getElementById("modal_producto_label").innerHTML = "Categoria: " + producto_detail['categoria'];; //ojo es de esta forma por ser un H2 html
    document.getElementById("modal_producto_titulo").innerHTML = producto_detail['nombre']; //ojo es de esta forma por ser un H2 html
    document.getElementById("modal_producto_descripcion").innerHTML = producto_detail['descripcion'];
    document.getElementById("modal-ppal-image").src = producto_detail['imagen'];


    let presentaciones = producto_detail['presentaciones'];
    $("#presentacion_select").empty();
    $("#presentacion_select").append("<option value=''>Seleccione</option>");
    for (i = 0; i < presentaciones.length; i++) {
      let presentacion = presentaciones[i];
      $('#presentacion_select').append("<option value='" + presentacion["pk"] + "'>" + presentacion["nombre_presentacion"] + "</option>").trigger("chosen:updated");
    }


    let tags = producto_detail['tags'];
    document.getElementById("modal_producto_tags").innerHTML = '';
    for (i = 0; i < tags.length; i++) {
      var tag = tags[i];
      document.getElementById("modal_producto_tags").innerHTML += `<a href="javascript:;" id-tag="${tag['nombre']}" >${tag['nombre']}</a><br>`;
    }

    $("#modal-producto").modal('show');
  })
  .catch((error) => {
    console.log('ERROR, ', error);
  });

  // let id_producto = e.getAttribute('id-producto');
  // // url = "{% url 'producto-detail' %}";
  // url = Urls.producto_detail();

  // $.get(url, {
  //   'id_producto': id_producto
  // }, function (data, status) {
  //   if (status == "success") {
  //     producto_detail = data['producto_detail'];
  //     tag_list = data['tag_list'];
  //     console.log("=====================");
  //     console.log(tag_list);
  //     var presentaciones_detail = data['presentaciones_detail'];

  //     document.getElementById("modal_producto_label").innerHTML = "Categoria: " + producto_detail[0]['categoria__nombre'];; //ojo es de esta forma por ser un H2 html
  //     document.getElementById("modal_producto_titulo").innerHTML = producto_detail[0]['nombre']; //ojo es de esta forma por ser un H2 html
  //     document.getElementById("modal_producto_descripcion").innerHTML = producto_detail[0]['descripcion'];
  //     document.getElementById("modal-ppal-image").src = "/media/" + `${producto_detail[0]['imagen']}`;
  //     $("#presentacion_select").empty();
  //     $("#presentacion_select").append("<option value=''>Seleccione</option>");
  //     for (i = 0; i < presentaciones_detail.length; i++) {
  //       var presentacion = presentaciones_detail[i];
  //       $('#presentacion_select').append("<option value='" + presentacion["pk"] + "'>" + presentacion["nombre_presentacion"] + "</option>").trigger("chosen:updated");
  //     }
  //     //Pinto los TAGS
  //     document.getElementById("modal_producto_tags").innerHTML = "";
  //     for (i = 0; i < tag_list.length; i++) {
  //       var name = tag_list[i].tags__nombre;
  //       console.log("fue...tag..." + name);
  //       document.getElementById("modal_producto_tags").innerHTML += `<a href="javascript:;" id-tag="${name}" onclick="buscar_x_tag(this)" >${name}</a><br>`;
  //     }

  //     $("#modal-producto").modal('show');
  //   } else {
  //     alert("Error de servidor contacte al administrador")
  //   }


  // })
}

$.fn.addCarrito = function () {
  var presentacion = $("#presentacion_select").val();
  var cantidad = $("#cantidad_select").val();
  var nombre_presentacion = $("#presentacion_select option:selected").text();

  // rectifico el pk del cliente-contacto
  // div_contacto.getAttribute("contacto-pk");

  index++;
  document.getElementById("modal_producto_precio").innerHTML = "";

  if (presentacion == '') {
    alert("Seleccione alguna presentación para continuar");
  } else {
    $("#modal-producto").modal('hide');
    var tr_producto = `
          <tr id="tr${index}" >
            <td>
              <h6 class="mb-0"><a href="#">${producto_detail[0]['nombre']}</a></h6>
              <p><small>Presentacion: ${nombre_presentacion} </small></p>
            </td>
            <td>
              <input class="form-control" type="number" value="${preciopresentacion}" disabled >
            </td>
            <td>
              <input class="form-control" type="number" name="pedido_cantidad" step="1" min="1" value="${cantidad}"  onchange="updateTotal();">
            </td>
            <td>$ ${preciopresentacion * cantidad}</td>
            <td class="text-center"><a href="javascript:;" onclick="$(this).removeProducto(this);" class="text-muted lead ml-1" data-id="${index}">
                  <i class="flaticon-delete-fill icon"> </i> </a>
                  <input type="hidden" name="pedido_presentacion" value="${presentacion}"
              </td>
          </tr>
          `;

    $("#tbody_pedido").append(tr_producto);
    document.getElementById('btn-tab-aux').click(); //show el tab de pedido
    index++;
    preciopresentacion = 0; //de vuelta a cero
    $("#cantidad_select").val(1);
    updateTotal()
  }
}

$.fn.removeProducto = function () {
  const id_producto = $(this).data('id');
  $('#tr' + id_producto).remove();
  updateTotal();

}

function updateTotal() {
  // alert("update total");
  var subtotal = 0;
  var total = 0;
  var descuento = 0;
  let div_contacto = document.getElementById('contacto');
  var pk_contacto = $('#id_contacto_pedido').val();
  // var pk_contacto = div_contacto.getAttribute("contacto-pk") //jalo el PK DEL CONTACTO PARA OTRA COSA
  console.log("pk contacto es..." + pk_contacto);
  $("#id_contacto_pedido").val(pk_contacto); //coloco el pk del contacto en el form del pedido
  table = document.getElementById("tbody_pedido");
  allrows = table.getElementsByTagName("tr");

  for (i = 0; i < allrows.length; i++) {
    console.log("Fila no_" + i);
    var precio = allrows[i].getElementsByTagName("input")[0].value;
    var cantidad = allrows[i].getElementsByTagName("input")[1].value;
    allrows[i]["cells"][3].innerText = "$ " + (precio * cantidad); //Actualizo la celda de subtotal
    console.log("precio " + precio + "cantidad " + cantidad);
    subtotal = subtotal + (precio * cantidad)

  }
  document.getElementById("pedido_subtotal").innerHTML = "$ " + subtotal;
  document.getElementById("pedido_total").innerHTML = "$ " + (subtotal - descuento);
  //VOY AL SERVISOR PARA ACTUALIZAR EL PEDIDO
  // alert("Actualizando...");
  let form_data = new FormData(document.getElementById('pedido_form'));
  url = Urls.pedido_crear();
  fetch(url, {
    method: 'post',
    body: form_data,
  })
    .then((response) => {
      console.log(response);
      return response.json();
    })
    .then((rta_server) => {
      // alert("Volvio bien .." + rta_server['pedido_pk']);
      console.log(rta_server['pedido_pk']);
      $("#id_pedido").val(rta_server['pedido_pk']);
    })

}

function consultar_pedido(gestion_pk) { //funcion sincrona

  //Voy a buscar el pedido por pk_gestion.
  console.log("buscando si hay pedido para gestion =====>>>" + gestion_pk);
  let url = Urls.pedido_consultar_ajax();
  $.get(url, {
    'gestion_pk': gestion_pk
  }, function (data, status) { // funcion asincrona Lo mando a aejecutar pero me toca tener cuidado que esto va y espera un tiempo mientras el resto se sigue ejecutando
    if (status == "success") {


      console.log("CONSULTA EXITOSA llego con el pedido no.. ", data['pedido']);
      // destruyo el body anterior
      row = document.getElementById('tbody_pedido')
      row.parentNode.removeChild(row);
      var new_tbody = document.createElement('tbody');
      new_tbody.id = `tbody_pedido`;
      $("#tabla_pedido").append(new_tbody)

      var pk_pedido = data['pedido'];
      if (pk_pedido != '') {
        $("#id_pedido").val(data['pedido']);
        $("#id_contacto_pedido").val(data['cliente_pk']);

        console.log("Reconstruyo la tabla ", data['item_list']);

        // <<<<<<<<<<<<<<<<<<<< RECONSTRUIR LA TABLA y el BODY  >>>>>>>>>>
        if (data['item_list'] != '') {
          data['item_list'].forEach(function (item, index) {
            console.log(item, index);
            var preciopresentacion = item['presentacion__precio_presentacion'];
            var cantidad = item['cantidad']
            var presentacion__pk = item['presentacion__pk']
            var tr_producto = `
              <tr id="tr${index}" >
                <td>
                  <h6 class="mb-0"><a href="#">${item['producto__nombre']}</a></h6>
                  <p><small>Presentacion: ${item['presentacion__nombre_presentacion']} </small></p>
                </td>
                <td>
                  <input class="form-control" type="number" value="${preciopresentacion}" disabled >
                </td>
                <td>
                  <input class="form-control" type="number" name="pedido_cantidad" step="1" min="1" value="${cantidad}"  onchange="updateTotal();">
                </td>
                <td>$ ${preciopresentacion * cantidad}</td>
                <td class="text-center"><a href="javascript:;" onclick="$(this).removeProducto(this);" class="text-muted lead ml-1" data-id="${index}">
                      <i class="flaticon-delete-fill icon"> </i> </a>
                      <input type="hidden" name="pedido_presentacion" value="${presentacion__pk}"
                  </td>
              </tr>
              `;
            $("#tbody_pedido").append(tr_producto);

          })
        }
        updateTotal();

      } else { //no tenia pedido entonces limpio pero le pongo el pk contacto
        $("#id_contacto_pedido").val(data['cliente_pk']);
        $("#id_pedido").val('');
        document.getElementById("pedido_subtotal").innerHTML = "$";
        document.getElementById("pedido_total").innerHTML = "$";

      }
      // $("#id_contacto_pedido").val(data['cliente_pk']);

    } else {
      console.log("consulta pedido fallo no conecto al server");
    }


  });
}

function crear_html_producto (producto_info) {
  /*
    producto_info = {
      'pk': 150,
      'nombre': 'hamburguesa',
      'valor': 9000,
      'imagen': '',
      'descripcion': 'Rica combinacion de carnes y pollos a la plancha',
      'serial': '1KFOTU89',
      'tags': [
        {'nombre': '#carneblandita'},
        {'nombre': '#carnemagra'},
        {'nombre': '#parasar'},
      ]
    }
  */

  const div_media = document.createElement('div');
  div_media.addEventListener('click', () => {
    detalle_producto(producto_info['pk']);
  });

  div_media.classList.value = 'media mb-4 mt-4 d-block d-sm-flex text-sm-left text-center';
  div_media.setAttribute('id', `producto-${producto_info['pk']}`);
  div_media.setAttribute('id-producto', producto_info['pk']);
  div_media.setAttribute('nombre-producto', producto_info['nombre']);
  div_media.setAttribute('valor-producto', producto_info['valor']);
  div_media.setAttribute('draggable', true);
  div_media.setAttribute('ondragstart', 'drag(event)');
  // div_media.addEventListener('dragstart', (e) => {
  //   alert('drag start');
  //   drag(e);
  // });

  const img_producto = document.createElement('img');
  img_producto.classList.value = 'mr-sm-2 mb-2 img-catalogo';
  if( producto_info['imagen'] == ''){
    img_producto.setAttribute('src', '/static/truecontact/template/assets/img/90x90.jpg');
  }else{
    img_producto.setAttribute('src', producto_info['imagen']);
  }

  div_media.appendChild(img_producto);

  const div_media_body = document.createElement('div');
  div_media_body.classList.value = 'media-body mb-2';
  div_media.appendChild(div_media_body);

  const nombre_producto = document.createElement('h5');
  nombre_producto.classList.value = 'media-heading mt-sm-0 mt-2';
  nombre_producto.textContent = producto_info['nombre'];
  div_media_body.appendChild(nombre_producto);

  const descripcion_producto = document.createElement('p');
  descripcion_producto.classList.value = 'media-text';
  descripcion_producto.textContent = producto_info['descripcion'].slice(0, 30) + '...';
  div_media_body.appendChild(descripcion_producto);

  const detalle_container = document.createElement('ul');
  detalle_container.classList.value = 'info-list list-unstyled mb-0';
  div_media_body.appendChild(detalle_container);

  const li_sku = document.createElement('li');
  li_sku.innerHTML = `
    <span class="text-danger">SKU:</span>
    <span class="text-muted">${producto_info['serial']}</span>
  `;
  detalle_container.appendChild(li_sku);

  const li_tags =  document.createElement('li');
  const tag_label = document.createElement('span');
  tag_label.classList.value = 'text-danger';
  tag_label.textContent = 'Tags:';
  li_tags.appendChild(tag_label);
  detalle_container.appendChild(li_tags);

  const tag_value = document.createElement('span');
  tag_value.classList.value = 'text-primary';

  lista_tags = producto_info['tags'];
  lista_tags.forEach((tag) => {
    let a = document.createElement('a');
    a.setAttribute('href', 'javascript:;');
    a.classList.value = 'text-primary';
    a.textContent = tag['nombre'] + ' ';
    tag_value.appendChild(a);
  });

  li_tags.appendChild(tag_value);

  return div_media;
}
