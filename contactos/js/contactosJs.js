
/* Elementos */
const btn_guardar_contacto = document.getElementById('btn-guardar-contacto');
// const btn_actualizar_contacto = document.getElementById('btn_actualizar_contacto');
const div_contacto_telefonia = document.getElementById('contacto');

const btn_rellamar =  document.getElementById('guardar-rellamar');

var index = 1

// var call_data_on_gestion = JSON.parse(div_contacto_telefonia.getAttribute('data-calldata'));  //esta variable global me permite operar con el calldata del contacto en egstion

/* Eventos */
document.addEventListener('DOMContentLoaded', (e) => {
  $("#selectresultados").empty();
  $("#selectresultados").append("<option value selected>---------</option>");
  let elementos_gestion = document.querySelectorAll('#main-left-col .phone');
  elementos_gestion.forEach((elemento) => {
    let id_gestion = elemento.getAttribute('data-gestion');
    elemento.addEventListener('click', () => {
      if(campana_nombre.value != "Conmutador"){
        cargar_gestion_contacto(id_gestion);
      } else {
        cargar_gestion_conmutador_contacto(id_gestion)
      }
    });
  });
});

// evento para llamar directo cuando seleccione ok en el modal de seleccion de campaña
const btn_modal_campana = document.getElementById('SelectCamp')
btn_modal_campana.addEventListener('click',()=>{
  $('#modalSelectCmp').modal('hide');
  manual_campaign_id = $('#cmpList').val();
  manual_campaign_type = $('#cmpList option:selected').attr('campana_type');
  var nombrecamp = $('#cmpList option:selected').html().trim();
  $('#campAssocManualCall').html(nombrecamp);
  var telefono = document.getElementById('numberToCall').value;
  const selectcampana = document.getElementById('cmpList');
  const idcampana = selectcampana.options[selectcampana.selectedIndex].value;
  const tipocampana = selectcampana.options[selectcampana.selectedIndex].getAttribute('campana_type')
 llamar_contacto(idcampana, tipocampana, '-1' , telefono);
 })



btn_rellamar.addEventListener('click', () => {

  let id_contacto = document.getElementById('id_contacto_gestion').value;

  if ( id_contacto == '') {
    swal.fire({
      icon: 'error',
      title: 'Error al guardar la Gestion',
      text: 'No se puede cargar una gestion a un Contacto desconocido, por favor actualice la informacion del contacto'
    })
  }else{

    setTimeout(() => { console.log("delay del guardar gestion ok!"); }, 2000);
    let url = Urls.tc_gestion_ajax_guardar();
    // let gestion = document.getElementById('id_gestion').value;
    let form_gestion = document.getElementById('form_agregar_gestion');

    if (form_gestion.checkValidity() == false) {
      form_gestion.classList.add('was-validated');
    }else{
      setTimeout(() => {
          $.post(url, $("#form_agregar_gestion").serialize()+"&finalizar=true", function (data, status) {
            let respuesta = data['respuesta'];
            let valido = data['valido'];
            if (valido == true) {
              $("#id_estado").val(data['estado']);
              console.log("llego de guardar la gestion" + data['estado']);
              $("#btn_guardar_gestion").attr("disabled", false);
              // alert("gestion terminada")
              Swal.fire({
                position: 'top-center',
                icon: 'info',
                title: 'Llamando de Nuevo',
                showConfirmButton: false,
                timer: 1500
              })

              document.getElementById("form_agregar_gestion").reset();  //Limpio el formulario bien

            }
            else {
              $("#guardar-gestion").attr("disabled", false);
              console.log(respuesta);
              swal.fire({
                icon: 'error',
                title: 'Accion con errores!',
                text: 'Se encontro un error al intentar cerrar la gestión, por favor comuniquese con el administrador del sistema'
              })
            }
            var div_gestion = data['div_gestion'];
            // alert("tenemos la variable de guardado" + call_id);
            let div_eliminar = document.getElementById(div_gestion);
            gestiones_agente.removeChild(div_eliminar);
            let call_data_string = div_contacto_telefonia.getAttribute('data-calldata');
            let call_data = JSON.parse(call_data_string);
            let id_campana = call_data['id_campana'];
            let tipo_campana = call_data['campana_type'];
            let id_contacto = call_data['id_contacto'];
            let telefono = call_data['telefono'];

            console.log(`campana: ${id_campana}, tipo: ${tipo_campana}, contacto: ${id_contacto}, telefono: ${telefono}`);
            console.log("Genero Llamada");
            var id_buttonre = document.getElementById('guardar-rellamar');
            id_buttonre.style.display='none';
            llamar_contacto(id_campana, tipo_campana, id_contacto, telefono);
          });
      }, 1000);
    }
  }
});

// si no existe el contacto enviar a que cree el contacto. si no fue capaz de crear el contactos
// inventar la forma que quede almacenado con un valor X
// en telefonia la gestion no se puede transferir
// Contacto prioridad - primero a la lista


div_contacto_telefonia.addEventListener('click', () => {
  if(campana_nombre.value != "Conmutador"){
    ver_contacto()
  } else {
    ver_directorio()
  }
  
});

btn_guardar_contacto.addEventListener('click', () => {
  if (document.getElementById('id_contacto').value != '' ){
    console.log("Contacto existente en tarjeta");
    update_contacto();

  }
  else{
    console.log("Nuevo contacto");
    crear_contacto();
  }
});

$('#guardar-gestion').click(() => {
  
  let id_contacto = document.getElementById('id_contacto_gestion').value;
  let url = Urls.tc_gestion_ajax_guardar();
  let form_gestion = document.getElementById('form_agregar_gestion');
  if (form_gestion.checkValidity() == false) {
    form_gestion.classList.add('was-validated');
  }else{
    $("#overlay").fadeIn(300);
    setTimeout(() => {
        $.post(url, $("#form_agregar_gestion").serialize()+"&finalizar=true", function (data, status) {
          let respuesta = data['respuesta'];
          let valido = data['valido'];
          if (valido == true) {
            $("#id_estado").val(data['estado']);
            console.log("llego de guardar la gestion" + data['estado']);
            $("#btn_guardar_gestion").attr("disabled", false);
            // alert("gestion terminada")
            swal.fire({
              icon: 'success',
              title: 'Gestión registrada!',
              text: `se ha finalizado la gestión de forma exitosa ${data['pk_gestion']}`
            })

            document.getElementById("form_agregar_gestion").reset();  //Limpio el formulario bien
            interfaz_whatsapp.disminuir_dialogos_gestion(1);

          }
          else {
            $("#guardar-gestion").attr("disabled", false);
            console.log(respuesta);
            swal.fire({
              icon: 'error',
              title: 'Accion con errores!',
              text: 'Se encontro un error al intentar cerrar la gestión, por favor comuniquese con el administrador del sistema'
            })
          }
          var div_gestion = data['div_gestion'];
          // alert("tenemos la variable de guardado" + call_id);
          let div_eliminar = document.getElementById(div_gestion);
          div_eliminar.style.display = "none";
          // gestiones_agente.removeChild(div_eliminar);
          // btn_encuesta.style.display == "none";
          url = Urls.tc_contacto()
          ver_crm(url);
          $("#overlay").fadeOut(300);
        });
    }, 2000);
  }
})

$('#btn-add-telefono-directorio').click(() => {
  crear_tr_telefono(document.getElementById("tabla-telefonos-directorio"))
})

$('#btn-add-telefono-directorio-detalle').click(() => {
  crear_tr_telefono(document.getElementById('tabla-telefonos-directorio-detalle'))
})

$('#cerrar_modal_crear_directorio').click(() => {
  document.getElementById("id_nombre_directorio").value = ""
  document.getElementById("tabla-telefonos-directorio").innerHTML = ""
})

$('#id_crear_directorio').click(async () => {

  let string_url = window.location.origin + Urls.tc_crear_directorio()

  let url = new URL(string_url)

  let data = {}
  data['nombre_directorio'] = document.getElementById("id_nombre_directorio").value
  if (data['nombre_directorio'] == "") {
    swal.fire({
      icon: 'error',
      title: 'Error en el nombre',
      text: "Por favor ingresa un nombre del directorio."
    })
    return 0
  }
  data['labels'] = []
  data['numeros'] =[]
  let labels = document.querySelectorAll('*[id^="id_label"]');
  let numeros = document.querySelectorAll('*[id^="id_directorio_telefono_"]')
  
  labels.forEach(label => {
    console.log(label.value)
    if (label.value){
      data['labels'].push(label.value)
    }
  })
  numeros.forEach(numero => {
    console.log(numero.value)
    if (numero.value){
      data['numeros'].push(numero.value)
    }
  })

  if(data['numeros'].length==0){
    swal.fire({
      icon: 'error',
      title: 'Error en los números',
      text: "Por favor ingresa al menos un número de teléfono."
    })
    return 0
  }

  if (data['labels'].length != data['numeros'].length){
    swal.fire({
      icon: 'error',
      title: 'Error en los números',
      text: "Por favor ingresa todos los sitios y números."
    })
    return 0
  }

  let response = await fetch(url, {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
        "Content-type": "application/json; charset=UTF-8",
        'X-CSRFToken': csrftoken
    }
  })

  let respuesta = await response.json()

  console.log(respuesta)

  if (respuesta['errors'] == true) {
    swal.fire({
      icon: 'error',
      title: 'Error en el nombre',
      text: respuesta['message']
    })
    return 0
  }

  swal.fire({
    icon: 'success',
    title: respuesta['message']
  })
  document.getElementById("id_nombre_directorio").value = ""
  document.getElementById("tabla-telefonos-directorio").innerHTML = ""
  modal = document.getElementById("addDirectorio")
  modal.classList.remove('show')
  modal.setAttribute('aria-hidden', 'true')
  modal.setAttribute('style', 'display: none')
  const modalsBackdrops = document.getElementsByClassName('modal-backdrop');
  for(let i=0; i<modalsBackdrops.length; i++) {
    document.body.removeChild(modalsBackdrops[i])
  }
  let select_directorio = document.getElementById('id_directorio_destino')
  let option  =document.createElement("option")
  option.text = respuesta['data']['nombre']
  option.value = respuesta['data']['id']
  select_directorio.add(option, select_directorio[1])
})

$('#id_directorio_destino').change(async () => {  
  select_directorio = document.getElementById('id_directorio_destino')
  let optionSelected = select_directorio.options[select_directorio.selectedIndex];

  let string_url = window.location.origin + Urls.tc_obtener_telefonos_directorio()
  let url = new URL(string_url), params = { 'id_directorio': optionSelected.value }
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  let select_telefono = document.getElementById("id_telefono")

  if (!optionSelected.value){
    select_telefono.innerHTML = "<option value='' selected>---------</option>"
  } else {
    let response =  await fetch(url, {method: 'GET'})
    let respuesta = await response.json()
    select_telefono.innerHTML = "<option value='' selected>---------</option>"
    respuesta['data'].forEach(dato => {
      let option = document.createElement("option")
      option.text = dato['numero']
      option.value = dato['id']
      select_telefono.add(option)
    })
  }
});

$('#form_agregar_gestion_conmutador').change(async () => {  
  id_gestion = document.getElementById("id_gestion_conmutador").value
  formulario_conmutador = document.getElementById('form_agregar_gestion_conmutador')
  data = new FormData(formulario_conmutador)
  dataForm = {}
  for (let [key, value] of data) {
    dataForm[key] = value
  }
  
  dataForm["finalizar"] = false

  let string_url = window.location.origin + Urls.tc_guardar_gestion_conmutador()
  let url = new URL(string_url)

  let response = await fetch(url, {
    method: "POST",
    body: JSON.stringify(dataForm),
    headers: {
        "Content-type": "application/json; charset=UTF-8",
        'X-CSRFToken': csrftoken
    }
  })

  let respuesta = await response.json()
  
})

$('#id_guardar_directorio').click(async () => {
  let call_data = JSON.parse(div_contacto_telefonia.getAttribute('data-calldata'));
  let string_url = window.location.origin + Urls.tc_modificar_directorio()

  let url = new URL(string_url)

  let data = {}
  data['id_directorio'] = call_data['id_directorio'];
  data['nombre_directorio'] = document.getElementById("id_nombre_directorio_detalle").value
  if (data['nombre_directorio'] == "") {
    swal.fire({
      icon: 'error',
      title: 'Error en el nombre',
      text: "Por favor ingresa un nombre del directorio."
    })
    return 0
  }
  if (data['nombre_directorio'] == "Desconocido") {
    swal.fire({
      icon: 'error',
      title: 'Error en el nombre',
      text: "Por favor cambia el nombre del directorio."
    })
    return 0
  }
  data['labels'] = []
  data['numeros'] =[]
  let labels = document.querySelectorAll('*[id^="id_label"]');
  let numeros = document.querySelectorAll('*[id^="id_directorio_telefono_"]')
  
  labels.forEach(label => {
    console.log(label.value)
    if (label.value){
      data['labels'].push(label.value)
    }
  })
  numeros.forEach(numero => {
    console.log(numero.value)
    if (numero.value){
      data['numeros'].push(numero.value)
    }
  })

  let response = await fetch(url, {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
        "Content-type": "application/json; charset=UTF-8",
        'X-CSRFToken': csrftoken
    }
  })

  let respuesta = await response.json()

  if (respuesta['errors'] == false) {
    swal.fire({
      icon: 'success',
      title: 'Directorio actualizado',
      text: respuesta['message']
    })
    let call_id = call_data['call_id'];
    let gestion = 'gestion-' + call_id
    let div = document.getElementById(gestion)
    let nombre_directorio = div_contacto_telefonia.getElementsByClassName('nombre-contacto')[0]
    nombre_directorio.textContent = document.getElementById("id_nombre_directorio_detalle").value
    let nombre = div.getElementsByClassName('nombre')[0]
    nombre.textContent = document.getElementById("id_nombre_directorio_detalle").value
    $("#modal-directorio").modal('hide');
  } else {
    swal.fire({
      icon: 'error',
      title: 'Nombre erróneo',
      text: respuesta['message']
    })
  }

})

$('#guardar_gestion_conmutadores').click(async () => {

  let nombre_directorio = div_contacto_telefonia.getElementsByClassName('nombre-contacto')[0].innerText

  if (nombre_directorio == 'Desconocido') { 
    swal.fire({
      icon: 'error',
      title: '¡Error!',
      text: 'Por favor actualiza el nombre del directorio.'
    })
    return 0
  }

  let formulario_conmutador = document.getElementById('form_agregar_gestion_conmutador')
  if (formulario_conmutador.checkValidity() == false) {
    formulario_conmutador.classList.add('was-validated')
  }else{
    $("#overlay").fadeIn(300)
    data = new FormData(formulario_conmutador)
    dataForm = {}
    for (let [key, value] of data) {
      dataForm[key] = value
    }
    dataForm["finalizar"] = true
    
    let string_url = window.location.origin + Urls.tc_guardar_gestion_conmutador()
    let url = new URL(string_url)

    let response = await fetch(url, {
      method: "POST",
      body: JSON.stringify(dataForm),
      headers: {
          "Content-type": "application/json; charset=UTF-8",
          'X-CSRFToken': csrftoken
      }
    })
  
    let respuesta = await response.json()

    if (respuesta['errors'] == false) {
      swal.fire({
        icon: 'success',
        title: respuesta['message'],
        text: `Se ha finalizado la gestión de forma exitosa ${respuesta['data']['id_gestion']}.`
      })
      formulario_conmutador.reset()
      let cantidad_gestiones = document.getElementById('cantidad-gestiones-agente')
      let numero = parseInt(cantidad_gestiones.innerText)
      numero -= 1
      cantidad_gestiones.textContent=numero.toString()
      let cantidad_gestiones2 = document.getElementById('cantidad-gestiones-agente_2')
      numero = parseInt(cantidad_gestiones2.innerText)
      numero -= 1
      cantidad_gestiones2.textContent=numero.toString()
    } else {
      swal.fire({
        icon: 'error',
        title: '¡Oh no!',
        text: 'Se encontró un error al intentar cerrar la gestión, por favor comuníquese con el administrador del sistema.'
      })
    }

    var div_gestion = respuesta['data']['div_gestion']
    let div_eliminar = document.getElementById(div_gestion)
    div_eliminar.style.display = "none"
    gestiones_agente.removeChild(div_eliminar)
    url = Urls.tc_gestiones_conmutador_list()
    ver_crm(url)
    $("#overlay").fadeOut(300);
  }
})


/* Funciones */

const crear_tr_telefono = tabla => {
  const tr_telefono = document.createElement('tr')
  tr_telefono.setAttribute('id', `tr_numero${index}`)
  tr_id = index
  const td_select_codigo = document.createElement('td')
  td_select_codigo.innerHTML = `
      <select name="label_telefonos_directorio" required class="form-control bm-4" id="id_label${index}">
          <option value="" selected="">-----</option>
          <option value="casa">Casa</option>
          <option value="oficina">Oficina</option>
          <option value="extension">Extension</option>
          <option value="otro">Otro</option>
      </select>
  `

  const td_input = document.createElement('td')
  const input = document.createElement('input')
  input.setAttribute('type', 'number')
  input.setAttribute('id', `id_directorio_telefono_${index}`)
  input.classList.add('form-control')

  input.addEventListener('change', e => {
      let numero_telefono = e.target.value
      consultar_telefono_directorio(numero_telefono, tr_id)
  })

  td_input.appendChild(input)

  tr_telefono.appendChild(td_select_codigo)
  tr_telefono.appendChild(td_input)

  const td_remover = document.createElement('td')
  td_remover.innerHTML = `<td class="text-center">
                              <a type="button" id="id_remover_numero_directorio_${index}" class="btn btn-outline-success" data-id="${index}">
                              Remover </a>
                          </td>`
  tr_telefono.appendChild(td_remover)

  tabla.appendChild(tr_telefono)

  let btn_remover_numero_directorio = document.getElementById(`id_remover_numero_directorio_${index}`)
  btn_remover_numero_directorio.addEventListener('click', () =>{
    tabla.removeChild(tr_telefono)
  })

  index++;
}

const ver_directorio = async () => {
  let nombre_directorio = document.querySelector('#id_nombre_directorio_detalle')
  nombre_directorio.value = ''
  let tabla_telefonos = document.querySelector('#tabla-telefonos-directorio-detalle')
  tabla_telefonos.innerHTML = ''

  let call_data = JSON.parse(div_contacto_telefonia.getAttribute('data-calldata'));
  let id_directorio = call_data['id_directorio'];

  if (!id_directorio=='') {
    let string_url = window.location.origin + Urls.tc_directorio_ver()
    let url = new URL(string_url), params = { 'id_directorio': id_directorio }
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))

    let response = await fetch(url, {method: 'get'})
    let respuesta = await response.json()

    let string_url2 = window.location.origin + Urls.tc_eliminar_telefono_directorio()
    let url2 = new URL(string_url2)

    nombre_directorio.value = respuesta["directorio_nombre"]
    respuesta["telefonos"].forEach(telefono => {
      let tr_telefono = document.createElement('tr')
      let td_select_codigo = document.createElement('td')
      let select = document.createElement('select')
      select.classList.add('form-control')
      select.innerHTML = `
        <option value="" selected="">-----</option>
        <option value="casa">Casa</option>
        <option value="oficina">Oficina</option>
        <option value="extension">Extension</option>
        <option value="otro">Otro</option>
      `;

      select.value = telefono['label']

      td_select_codigo.appendChild(select)

      let td_input = document.createElement('td')
      let input = document.createElement('input')
      input.setAttribute('type', 'number')
      input.setAttribute('id', `id_telefono_directorio_detalle_${telefono["id"]}`)
      input.classList.add('form-control')

      input.value = telefono["numero"]

      input.addEventListener('change', e => {
          let numero_telefono = e.target.value
          consultar_telefono_directorio(numero_telefono, tr_id)
      })

      td_input.appendChild(input);

      tr_telefono.appendChild(td_select_codigo);
      tr_telefono.appendChild(td_input)

      const td_remover = document.createElement('td');
      td_remover.innerHTML = `<td class="text-center">
                                  <a type="button" id="id_remover_numero_directorio_detalle_${telefono["id"]}" class="btn btn-outline-success" data-id="${telefono["id"]}">
                                  Remover </a>
                              </td>`;

      tr_telefono.appendChild(td_remover);

      document.getElementById('tabla-telefonos-directorio-detalle').appendChild(tr_telefono)

      let btn_remover_numero_directorio = document.getElementById(`id_remover_numero_directorio_detalle_${telefono["id"]}`)
      btn_remover_numero_directorio.addEventListener('click', async () =>{
        
        let response = await fetch(url2, {
          method: "POST",
          body: JSON.stringify({'id_telefono' : telefono["id"]}),
          headers: {
              "Content-type": "application/json; charset=UTF-8",
              'X-CSRFToken': csrftoken
          }
        })
      
        let respuesta = await response.json()
        if (respuesta['errors'] == false) {
          swal.fire({
            icon: 'success',
            title: respuesta['message'],
            text: 'Se ha eliminado correctamente el número de teléfono.'
          })
          document.getElementById('tabla-telefonos-directorio-detalle').removeChild(tr_telefono)
        } else {
          swal.fire({
            icon: 'error',
            title: respuesta['message'],
            text: 'El número que se quiere eliminar no existe.'
          })
        }
      })

    })
  }
  $("#modal-directorio").modal('show');
}

const cargar_telefono = async (id_directorio, id_telefono) => {
  let select_telefono = document.getElementById("id_telefono")
  select_telefono.innerHTML = "<option value='' selected>---------</option>"

  if (id_directorio) {
    let string_url = window.location.origin + Urls.tc_obtener_telefonos_directorio()
    let url = new URL(string_url), params = { 'id_directorio': id_directorio }
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
    let response =  await fetch(url, {method: 'GET'})
    let respuesta = await response.json()
    respuesta['data'].forEach(dato => {
      let option = document.createElement("option")
      option.text = dato['numero']
      option.value = dato['id']
      select_telefono.add(option)
    })
    select_telefono.value = id_telefono
  }  
}

const identificar_directorio_llamada = async call_data => {
  let string_url = window.location.origin + Urls.tc_identificar_directorio();
  let url = new URL(string_url), params = { 'call_data': JSON.stringify(call_data) };
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  let response = await fetch(url, {method: 'GET'})

  let datos = await response.json()

  let directorio = datos;

  call_data['id_directorio'] = directorio['id'];
  agregar_gestion_conmutador_telefonia(call_data, directorio);
}

const agregar_gestion_conmutador_telefonia = async (call_data, directorio) => {  //se ejecuta cuando contesto la llamada gracias a OMNILEADS

  let string_url = window.location.origin + Urls.tc_conmutador_gestionar();
  let url = new URL(string_url), params = {'call_data': JSON.stringify(call_data)}
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  let date = new Date();
  let data_llamada = '';

  let response = await fetch(url, {method:'GET'})

  let data = await response.json()

  let datos = data['respuesta'];

  let id_gestion = datos['gestion'];
  call_data['id_gestion'] = id_gestion;
  div_contacto_telefonia.setAttribute('data-id', call_data['call_id']);
  div_contacto_telefonia.setAttribute('data-calldata', JSON.stringify(call_data));

  data_llamada = {
    'id': call_data['call_id'],
    'estado': 'en_gestion',
    'medio': call_data['medio'],
    'nombre_corto': directorio['nombre'] == "Desconocido"? call_data['telefono'] : directorio['nombre'],
    'mensajes_nuevos': '1',
    'imagen': '',
    'fecha_modificacion': date.toLocaleString('co'),
    'id_gestion': id_gestion,
    'nombre_campana':datos['nombre_campana'],
  }

  div_llamada = crear_html_gestion(data_llamada)

  const first_child = gestiones_agente.firstElementChild
  gestiones_agente.insertBefore(div_llamada, first_child)

  let cantidad_gestiones = document.getElementById('cantidad-gestiones-agente')
  let numero = parseInt(cantidad_gestiones.innerText)
  numero += 1
  cantidad_gestiones.textContent=numero.toString()

  let cantidad_gestiones2 = document.getElementById('cantidad-gestiones-agente_2')
  numero = parseInt(cantidad_gestiones2.innerText)
  numero += 1
  cantidad_gestiones2.textContent=numero.toString()

  const imagen_contacto = div_contacto_telefonia.getElementsByClassName('imagen-contacto')[0]
  imagen_contacto.src = '/static/truecontact/truecontact/img/unknow-user.png'

  const nombre_contacto = div_contacto_telefonia.getElementsByClassName('nombre-contacto')[0]
  nombre_contacto.textContent = directorio['nombre']

  cargar_gestion_conmutador_contacto(datos['gestion'])
  identificar_medio(call_data)
  select_this_box(call_data['call_id']); //sombreo la gestion de LLamada
  var global_origen = window.parent.document.getElementById('modelo_origen_gestion')
  var dic_modelo = {'origen': 'None', 'pk': ''}
  var dic_nuevo = JSON.stringify(dic_modelo)
  global_origen.value = dic_nuevo
}

const cargar_gestion_conmutador_contacto = async id_gestion => {
// console.log("#################################################");
// console.log("##Entro a la funcion cargar_gestion_conmutador.##");
// console.log("#################################################");

  let form_contacto = document.getElementById("form-contacto");
  form_contacto.reset();

  let form_gestion = document.getElementById('form_agregar_gestion_conmutador');
  if (form_gestion.classList.contains('was-validated')) {
    form_gestion.classList.remove('was-validated')
  }

  let string_url = window.location.origin + Urls.tc_cargar_gestion_conmutador();
  let url = new URL(string_url), params = { 'id_gestion': id_gestion };

  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  let response =  await fetch(url, {method: 'GET'})
  let data = await response.json()

  let respuesta = data['respuesta']

  document.getElementById("id_gestion_conmutador").value = respuesta['id']
  document.getElementById("id_directorio_gestion").value = respuesta['id_directorio'] // Le atribuyo al formulario de conmutador el pk del directorio
  document.getElementById("id_metadata_conmutador").value = respuesta['metadata']

  $("#id_tipo").val(respuesta['tipo']);
  $("#tipo-label").text($("#id_tipo option:selected").text());
  $("#id_medio").val(respuesta['medio']);
  $("#medio-label").text($("#id_medio option:selected").text());

  document.getElementById("id_nombre").value = respuesta['nombre']
  document.getElementById("id_directorio_destino").value = respuesta['directorio_destino']
  cargar_telefono(respuesta['directorio_destino'], respuesta['telefono'])
  document.getElementById("id_resultado").value = respuesta['resultado']
  document.getElementById("id_observaciones").value = respuesta['observaciones']

  // Entonces el div superior del CONTACTO lo actualizo
  id_directorio = respuesta['id_directorio'];
  contacto_full_name = respuesta['directorio_full_name'];
  imagen = '';

  div_contacto_telefonia.setAttribute('contacto-pk', id_directorio);
  div_contacto_telefonia.setAttribute('id-gestion', id_gestion);

  const imagen_contacto = div_contacto_telefonia.getElementsByClassName('imagen-contacto')[0];
  if (imagen == '') {
    imagen_contacto.setAttribute('src', '/static/truecontact/truecontact/img/unknow-user.png');
  } else {
    imagen_contacto.setAttribute('src', imagen);
  }

  let call_data = respuesta['call_data']
  call_data['id_gestion'] = id_gestion

  const nombre_contacto = div_contacto_telefonia.getElementsByClassName('nombre-contacto')[0];
  nombre_contacto.textContent = contacto_full_name;
  
  switch (call_data['medio']) {
    case 'telefonia':
      div_contacto_telefonia.setAttribute('data-id', call_data['call_id']);
      break;
    case 'whatsapp':
      div_contacto_telefonia.setAttribute('data-id', call_data['chat_id']);
      break;
    default:
      div_contacto_telefonia.setAttribute('data-id', '');
  }

  div_contacto_telefonia.setAttribute('data-calldata', JSON.stringify(call_data));

  identificar_medio(call_data);

    // Funcion que ubica el historial de gestiones
    let historial = respuesta['historial'];

    if (historial.length > 1){
      document.getElementById("btn-ver-ultima-gestion").style.removeProperty('display') //to show button
      document.getElementById("btn-ver-ultima-gestion").setAttribute("onclick",`verGestion(${respuesta['historial'][0]['id']}, false)`)
      document.getElementById("tbody_historial").innerHTML = '';
      respuesta['historial'].forEach(function (item, indexx) {
      //  console.log(item, index);
        var tr_historial = `
          <tr id="tr${indexx}" >
            <td onClick="parent.verGestion(${item['id']}, false)" >
              <h6 class="mb-0">${item['medio']}</h6>
              <p><small>${item['fecha_creacion']}</small></p>
              <h6 class="mb-0">Estado: ${item['estado']}</h6>
            </td>
          </tr>
          `;
        $("#tbody_historial").append(tr_historial);

      })
    }
    else{
    document.getElementById("tbody_historial").innerHTML = '';
    document.getElementById("btn-ver-ultima-gestion").style.display='none'; //to hide button
    }
    // END Historial gestiones
}

function identificar_contacto_llamada(call_data) {
  let string_url = window.location.origin + Urls.tc_contacto_identificar();
  let url = new URL(string_url),
    params = { 'call_data': JSON.stringify(call_data) };
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  fetch(url, {
    method: 'get',
  })
    .then((response) => {
      return response.json();
    })
    .then((datos) => {
      let cantidad_contactos = datos['cantidad_contactos'];
      let lista_contactos = datos['contactos'];


      if (cantidad_contactos == 0) {
        contacto = {
          'imagen': '',
          'nombre_corto': 'Desconocido',
          'nombre_largo': 'Desconocido',
          'documento': '',
        }
        call_data['id_contacto'] = '';
        console.log('>>> No tiene contacto relacionado. <<<')
        agregar_gestion_telefonia(call_data, contacto);

      } else if (cantidad_contactos == 1) {
        contacto = lista_contactos[0];
        call_data['id_contacto'] = contacto['id'];
        agregar_gestion_telefonia(call_data, contacto);
        console.log('>>> Si tiene contacto relacionado. <<<')
      } else {
        console.log('Se identifico VARIOS CONTACTOS');
        contacto = {
          'imagen': '',
          'nombre_corto': 'Seleccionar Contacto',
          'nombre_largo': 'Seleccionar Contacto',
        }
        console.log('>>> Tiene varios contacto relacionados. <<<')
        call_data['id_contacto'] = '';
        agregar_gestion_telefonia(call_data, contacto); // aqui vamos Andres y William

        // destruyo el body anterior
        row = document.getElementById('tbody_contactos')
        row.parentNode.removeChild(row)
        var new_tbody = document.createElement('tbody');
        new_tbody.id = `tbody_contactos`;
        $("#tabla_contactos").append(new_tbody)

        datos['contactos'].forEach(function (item, index) {
          console.log("traemos los contactos" );
          console.log(item, index);
          var nombre_largo = item['nombre_largo']
          var nombre_corto = item['nombre_corto']
          var contacto_pk = item['id']
          var tr_contacto =`
          <tr id="tr${contacto_pk}" >
            <td>
              ${nombre_largo}
            </td>
            <td class="text-center"><a href="javascript:;" onclick="$(this).gestionContacto(this);" class="text-muted lead ml-1" data-contacto_id="${contacto_pk}" data-nombre_corto="${nombre_corto}" data-nombre_largo="${nombre_largo}" >
                  <i class="flaticon-log icon"> </i> </a>
              </td>
          </tr>
          `;
        $("#tbody_contactos").append(tr_contacto);
        $("#modal-ver-contactos").modal('show');


        })
      }


    })
    .catch((error) => {
      console.log('ERROR, ', error);
    });
}

function agregar_gestion_telefonia(call_data, contacto) {  //se ejecuta cuando contesto la llamada gracias a OMNILEADS

  let url = Urls.tc_contacto_ajax_gestionar();
  let call_data_json = JSON.stringify(call_data);
  let date = new Date();
  let data_llamada = '';

  $.get(url, { call_data_json }, function (data, status) {  // funcion asincrona Lo mando a aejecutar pero me toca tener cuidado que esto va y espera un tiempo mientras el resto se sigue ejecutando
    if (status == "success") {

      let datos = data['respuesta'];

      let id_gestion = datos['gestion'];
      call_data['id_gestion'] = id_gestion;
      div_contacto_telefonia.setAttribute('data-id', call_data['call_id']);
      div_contacto_telefonia.setAttribute('data-calldata', JSON.stringify(call_data));
      document.getElementById("id_gestioncontacto").value = datos['gestion']; //para tomar bien el pedido del nuevo

      data_llamada = {
        'id': call_data['call_id'],
        'estado': 'en_gestion',
        'medio': call_data['medio'],
        'nombre_corto': contacto['nombre_corto'],
        'mensajes_nuevos': '1',
        'imagen': contacto['imagen'],
        'fecha_modificacion': date.toLocaleString('co'),
        'id_gestion': id_gestion,
        'nombre_campana':datos['nombre_campana'],
      }

      div_llamada = crear_html_gestion(data_llamada);

      const first_child = gestiones_agente.firstElementChild;
      gestiones_agente.insertBefore(div_llamada, first_child);
      interfaz_whatsapp.aumentar_dialogos_gestion(1);

      const imagen_contacto = div_contacto_telefonia.getElementsByClassName('imagen-contacto')[0];
      if (contacto['imagen'] == '') {
        imagen_contacto.src = '/static/truecontact/truecontact/img/unknow-user.png';
      } else {
        imagen_contacto.src = contacto['imagen'];
      }

      const nombre_contacto = div_contacto_telefonia.getElementsByClassName('nombre-contacto')[0];
      nombre_contacto.textContent = contacto['nombre_largo'];

      cargar_gestion_contacto(datos['gestion']);
      identificar_medio(call_data);//ANtes debo renderizar el form
      select_this_box(call_data['call_id']); //sombreo la gestion de LLamada
      var global_origen = window.parent.document.getElementById('modelo_origen_gestion');
      var dic_modelo = {'origen': 'None', 'pk': ''}
      var dic_nuevo = JSON.stringify(dic_modelo);
      global_origen.value = dic_nuevo;
      console.log('borreeee el valor de origen de gestion', global_origen.value)
    } else {
      alert("Error del servidor consulte al administrador");
    }
  });
}


function cargar_gestion_contacto(id_gestion) {
  // console.log("#################################################");
  // console.log("###Entro a la funcion cargar_gestion_contacto####");
  // console.log("#################################################");

  let form_contacto = document.getElementById("form-contacto");
  form_contacto.reset();

  let form_gestion = document.getElementById('form_agregar_gestion');
  if (form_gestion.classList.contains('was-validated')) {
    form_gestion.classList.remove('was-validated')
  }

  let string_url = window.location.origin + Urls.tc_gestion_cargar();
  let url = new URL(string_url),
    params = { 'id_gestion': id_gestion };

  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

fetch(url, {
    method: 'get',
  })
    .then((response) => {
      return response.json();
    })
    .then((data) => {

      // INICIO Andresito JQUERY

      let respuesta = data['respuesta'];
      document.getElementById("id_gestion").value = (respuesta['id']);


      // btn_encuesta.style.display='none';


      $("#id_contacto").val(respuesta['id_contacto']);
      $("#id_contacto_gestion").val(respuesta['id_contacto']); // Le pego al FORMULARIO GESTION el pk contacto
      $("#id_metadata").val(respuesta['metadata']);
      $("#id_tipo").val(respuesta['tipo']);
      $("#tipo-label").text($("#id_tipo option:selected").text());

      actualizarResultado1(respuesta['motivo'],respuesta['resul']);

      $("#id_medio").val(respuesta['medio']);
      $("#medio-label").text($("#id_medio option:selected").text());
      //$("#id_resultado").val(respuesta['resultado']); LO PONE LA FUNCION
      //$("#id_calificacion").val(respuesta['calificacion']);
      //$("#id_observaciones").val(respuesta['observaciones']);
      let tyc = respuesta['tyc']==null ? "unknown" : `${respuesta['tyc']}`;
      $("#selectmotivos").val(respuesta['motivo']);
      $("#selectespecialidad").val(respuesta['especialidad']);
      $("#selectentidad").val(respuesta['entidad']);
      $("#selecttipoconsulta").val(respuesta['tipo_consulta']);
      if (respuesta['hechotyc'] == true){
        $("#label_tyc").hide();
        $("#selecttyc").hide();
      }

      $("#selecttyc").val(tyc);
      $("#id_observaciones").val(respuesta['observaciones']);
      medio = document.getElementById('medio-label');

      if (medio.innerText == 'Telefonia'){
        btn_trans = document.getElementById('swal_fire_transferir');
        btn_trans.style.display = 'none';

      }
      if(medio.innerText == 'WhatsApp'){
        btn_trans = document.getElementById('swal_fire_transferir');
        btn_trans.style.display = 'inline-block';
      }

      // Entonces el div superior del CONTACTO lo actualizo
      contacto_pk = respuesta['contacto_pk'];
      contacto_full_name = respuesta['contacto_full_name'];
      imagen = respuesta['contacto_imagen'];

      div_contacto_telefonia.setAttribute('contacto-pk', contacto_pk);
      div_contacto_telefonia.setAttribute('id-gestion', id_gestion);

      const imagen_contacto = div_contacto_telefonia.getElementsByClassName('imagen-contacto')[0];
      if (imagen == '') {
        imagen_contacto.setAttribute('src', '/static/truecontact/truecontact/img/unknow-user.png');
      } else {
        imagen_contacto.setAttribute('src', imagen);
      }

      let call_data = respuesta['call_data'];
      call_data['id_gestion'] = id_gestion;

      const nombre_contacto = div_contacto_telefonia.getElementsByClassName('nombre-contacto')[0];
      nombre_contacto.textContent = contacto_full_name;
      
      switch (call_data['medio']) {
        case 'telefonia':
          div_contacto_telefonia.setAttribute('data-id', call_data['call_id']);
          break;
        case 'whatsapp':
          div_contacto_telefonia.setAttribute('data-id', call_data['chat_id']);
          break;
        default:
          div_contacto_telefonia.setAttribute('data-id', '');
      }

      div_contacto_telefonia.setAttribute('data-calldata', JSON.stringify(call_data));

      //entonces pongo el pk del pedido en FORM_PEDIDO
      //$("#id_gestioncontacto").val(respuesta['id']); //coloco el pk dela gestion el form del pedido
      //VOY A COLSULTAR SI EXISTEUN PEDIDO CREADO y lo renderiso
      //consultar_pedido(respuesta['id']);

      let info_programacion = respuesta['info_programacion'];

      $("#contacto_encuesta").val(respuesta['id_contacto']);
      $("#gestion_encuesta").val(respuesta['id']);


      $("#programacion_encuesta").val(info_programacion['data']['id']);

      console.log(">>> cargar_gestion_contacto > Si tiene programaciones ", info_programacion );

      if(info_programacion['has_data'] == true){
        document.getElementById('programacion-info').style.display = 'block';
        document.getElementById('info-documento').textContent = info_programacion['data']['documento_contacto'];
        document.getElementById('info-doctor').textContent = info_programacion['data']['doctor'];
        document.getElementById('info-tipoes').textContent = info_programacion['data']['tipoes'];
        document.getElementById('info-entidad').textContent = info_programacion['data']['entidad'];
        document.getElementById('info-fechapro').textContent = info_programacion['data']['fechapro'];
        document.getElementById('info-fechacit').textContent = info_programacion['data']['fechacit'];

        let telefonos = respuesta['telefono'];
        document.getElementById("numeo-telefono").innerHTML = '';
        for (i = 0; i < telefonos.length; i++) {
          var telefono = telefonos[i];
          if  (i==(telefonos.length-1)){
            document.getElementById("numeo-telefono").textContent += telefono['telefono']
          }else{
          document.getElementById("numeo-telefono").textContent += telefono['telefono']+ ', '
                }}

    }else{
        document.getElementById('programacion-info').style.display = 'none';
        document.getElementById('info-documento').textContent = '';
        document.getElementById('info-observaciones').textContent = '';
        document.getElementById('numeo-telefono').textContent = '';
      }

      identificar_medio(call_data);

       // Funcion que ubica el historial de gestiones
      //  console.log("HISTORIAL ES: ", respuesta['historial']);
       let historial = respuesta['historial'];
       if (historial.length > 1){
         document.getElementById("btn-ver-ultima-gestion").style.removeProperty('display') //to show button
         document.getElementById("btn-ver-ultima-gestion").setAttribute("onclick",`verGestion(${respuesta['historial'][0]['id']}, false)`)
         document.getElementById("tbody_historial").innerHTML = '';
         respuesta['historial'].forEach(function (item, indexx) {
           var tr_historial = `
             <tr id="tr${indexx}" >
               <td onClick="parent.verGestion(${item['id']}, false)" >
                 <h6 class="mb-0">${item['medio']}</h6>
                 <p><small>${item['fecha_creacion']}</small></p>
                 <h6 class="mb-0">Estado: ${item['estado']}</h6>
               </td>
             </tr>
             `;
           $("#tbody_historial").append(tr_historial);

         })
       }
       else{
        document.getElementById("tbody_historial").innerHTML = '';
        document.getElementById("btn-ver-ultima-gestion").style.display='none'; //to hide button
       }
       // END Historial gestiones

    })
    .catch((error) => {
      console.log('ERROR, ', error);
    });
}

function ver_contacto() {
  console.log("ver contacto....");
  let img_contacto = div_contacto_telefonia.getElementsByClassName('imagen-contacto')[0];
  let img_src_contacto = img_contacto.getAttribute('src');

  let img_contacto_update = document.getElementById('profile-image-upload');
  img_contacto_update.setAttribute('src', img_src_contacto);

  let form_contacto = document.getElementById("form-contacto");

  $("#id_industria").val(); //Esto es de la libreria de select 2 toca con jquery
  $("#id_industria").trigger('change'); //Esto es de la libreria de select 2 toca con jquery
  let table_telefonos_body = document.querySelector('#contacto-telefonos tbody');
  table_telefonos_body.innerHTML = '';
  let table_emails_body = document.querySelector('#contacto-emails tbody');
  table_emails_body.innerHTML = '';
  let table_direcciones_body = document.querySelector('#contacto-direcciones tbody');
  table_direcciones_body.innerHTML = '';

  let call_data = JSON.parse(div_contacto_telefonia.getAttribute('data-calldata'));
  console.log("El call data que trae es: "+call_data)
  let id_contacto = call_data['id_contacto'];

  if (id_contacto == '') {

  }else {
    let string_url = window.location.origin + Urls.tc_contacto_ajax_ver();
    let url = new URL(string_url),
      params = { 'pk_contacto': id_contacto };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

    fetch(url, {
      method: 'get'
    })
    .then(function (res) {
      return res.json();
    })
    .then(function (respuesta) {
      const contacto = respuesta['contacto'];
      // console.log("Contacto..", contacto)
      document.getElementById('id_tipo_identificacion').value = contacto['tipo_identificacion'];
      document.getElementById('id_documento').value = contacto['documento'];
      document.getElementById('id_nombres').value = contacto['nombres'];
      document.getElementById('id_apellidos').value = contacto['apellidos'];
      $("#id_industria").val(contacto['industria']); //Esto es de la libreria de select 2 toca con jquery
      $("#id_industria").trigger('change'); //Esto es de la libreria de select 2 toca con jquery
      document.getElementById('id_es_compania').checked = contacto['es_compania'];
      document.getElementById('id_bool_1').checked = contacto['es_cliente'];
      document.getElementById('id_bool_2').checked = contacto['es_proveedor'];
      document.getElementById('id_bool_3').checked = contacto['posible_cliente'];
      document.getElementById('id_observaciones_contacto').value = contacto['observaciones'];

      telefonos = contacto['telefonos'];
      emails = contacto['emails'];
      direcciones = contacto['direcciones'];


      telefonos.forEach((telefono) => {
        let tr_telefono = crear_html_telefono(telefono);
        table_telefonos_body.appendChild(tr_telefono);
      });

      emails.forEach((email) => {
        let tr_email = crear_html_email(email);
        table_emails_body.appendChild(tr_email);
      });

      direcciones.forEach((direccion) => {
        let tr_direccion = crear_html_direccion(direccion);
        table_direcciones_body.appendChild(tr_direccion);
      });

    })
    .catch(function (error) {
      console.log(error);
    });
  }

  $("#modal-contacto").modal('show');
}

function crear_contacto(){

  let url = Urls.tc_contacto_add_ajax();

  let form_contacto = document.getElementById('form-contacto');
  let call_data_json = div_contacto_telefonia.getAttribute('data-calldata');
  let call_data = JSON.parse(call_data_json);

  let form_data = new FormData(form_contacto);
  form_data.append('id_contacto', call_data['id_contacto']);
  form_data.append('call_data', call_data_json);

  if (form_contacto.checkValidity() == false) {
    form_contacto.classList.add('was-validated');
  }else{
    fetch(url, {
      method: 'post',
      body: form_data,
    })
    .then((res) => {
      return res.json();
    })
    .then((datos) => {
      let status = datos['status'];
      $("#modal-contacto").modal('hide');
      if (status == 'ok'){
        let contacto = datos['contacto'];

        let call_data = JSON.parse(div_contacto_telefonia.getAttribute('data-calldata'));

        call_data['id_contacto'] = contacto['id'];
        console.log('>>>> crear_contacto. call_data[id_contacto]: ', call_data['id_contacto'])
        console.log('>>>> crear_contacto. contacto[id]: ', contacto['id'])
        $("#id_contacto_gestion").val(contacto['id']); // Le pego al FORMULARIO GESTION el pk contacto

        div_contacto_telefonia.setAttribute('data-calldata', JSON.stringify(call_data));

        document.getElementById('id_contacto_gestion').value = call_data['id_contacto'];
        document.getElementById('id_metadata').value = JSON.stringify(call_data);
        actualizar_gestion();

        let data_id = div_contacto_telefonia.getAttribute('data-id');
        let div_gestion = document.getElementById(`gestion-${data_id}`);

        const imagen_contacto = div_contacto_telefonia.getElementsByClassName('imagen-contacto')[0];
        let div_gestion_imagen = div_gestion.getElementsByClassName('img-user-dialogo')[0];
        if (contacto['imagen'] != '') {
          imagen_contacto.src = contacto['imagen'];
          div_gestion_imagen.src = contacto['imagen'];
        }

        const nombre_contacto = div_contacto_telefonia.getElementsByClassName('nombre-contacto')[0];
        let div_gestion_nombre = div_gestion.getElementsByClassName('nombre')[0];

        nombre_contacto.textContent = contacto['nombre_largo'];
        div_gestion_nombre.textContent = contacto['nombre_corto'];

        swal.fire({
          icon: 'success',
          title: 'Accion exitosa!',
          text: `se ha guardado el contacto de forma exitosa`
        })
      }else{
        swal.fire({
          icon: 'error',
          title: 'Accion con errores!',
          text: 'Se encontro un error al intentar guardar el contacto, por favor comuniquese con el administrador del sistema'
        })
      }
    })
    .catch((error) => {
      console.log('ERROR: ', error);
    });
  }
}

function update_contacto(){

  let url = Urls.tc_contacto_ajax_update();

  let form_contacto = document.getElementById('form-contacto');
  let call_data_json = div_contacto_telefonia.getAttribute('data-calldata');
  let call_data = JSON.parse(call_data_json);

  let form_data = new FormData(form_contacto);
  form_data.append('call_data', call_data_json);

  if (form_contacto.checkValidity() == false) {
    form_contacto.classList.add('was-validated');
  }else{
    fetch(url, {
      method: 'post',
      body: form_data,
    })
    .then((res) => {
      return res.json();
    })
    .then((datos) => {
      let status = datos['status'];
      $("#modal-contacto").modal('hide');
      if (status == 'ok'){
        let contacto = datos['contacto'];

        let call_data = JSON.parse(div_contacto_telefonia.getAttribute('data-calldata'));

        call_data['id_contacto'] = contacto['id'];
        $("#id_contacto_gestion").val(contacto['id']); // Le pego al FORMULARIO GESTION el pk contacto

        div_contacto_telefonia.setAttribute('data-calldata', JSON.stringify(call_data));

        document.getElementById('id_contacto_gestion').value = call_data['id_contacto'];
        document.getElementById('id_metadata').value = JSON.stringify(call_data);
        actualizar_gestion();

        let data_id = div_contacto_telefonia.getAttribute('data-id');
        let div_gestion = document.getElementById(`gestion-${data_id}`);

        const imagen_contacto = div_contacto_telefonia.getElementsByClassName('imagen-contacto')[0];
        let div_gestion_imagen = div_gestion.getElementsByClassName('img-user-dialogo')[0];
        if (contacto['imagen'] != '') {
          imagen_contacto.src = contacto['imagen'];
          div_gestion_imagen.src = contacto['imagen'];
        }

        const nombre_contacto = div_contacto_telefonia.getElementsByClassName('nombre-contacto')[0];
        let div_gestion_nombre = div_gestion.getElementsByClassName('nombre')[0];

        nombre_contacto.textContent = contacto['nombre_largo'];
        div_gestion_nombre.textContent = contacto['nombre_corto'];

        swal.fire({
          icon: 'success',
          title: 'Accion exitosa!',
          text: `se ha Actualizado el contacto de forma exitosa`
        })
      }else{
        swal.fire({
          icon: 'error',
          title: 'Accion con errores!',
          text: 'Se encontro un error al intentar guardar el contacto, por favor comuniquese con el administrador del sistema'
        })
      }
    })
    .catch((error) => {
      console.log('ERROR: ', error);
    });
  }
}

function actualizar_form_gestion() {
  var gestion = $('#id_gestion').val();
  console.log("formulario cambiando llamo a guardado automatico. GESTION: " + gestion);
  let url = Urls.tc_gestion_ajax_guardar();

  if (gestion != '') {
    console.log("Tiene una gestion asociada").
    $.post(url, $("#form_agregar_gestion").serialize(), function (data, status) {
      var respuesta = data['respuesta'];
      var valido = data['valido'];

      if (valido == true) {
        $("#id_estado").val(data['estado']);
        console.log("llego de guardar la gestion" + data['estado']);
      }
      else {
        console.log(respuesta);
      }
    });
  }
  else {
    console.log("No hay gestion asociada fallo al guardar contactosJS")

  }
}

//cargar gestion telefonia
function cargar_gestion_telefonia(id_telefonia) {
  let url = `/tc/gestion/ajax/telefonia/?call_id=${id_telefonia}`;
  fetch(url)
    .then(function (res) {
      return res.json();
    })
    .then(function (respuesta) {
      // const gestion = document.getElementById(`gestion-${id_telefonia}`);
      const gestion = respuesta['gestion'];

      let call_data = gestion['call_data'];
      // console.log("OJOXXX", call_data['medio'] );
      if (call_data['medio'] == 'whatsapp') {
        div_chat_system.style.display = 'block';
        div_phone_system.style.display = 'none';
        div_gestion_system.style.display = 'block';
      }
      else {
        div_chat_system.style.display = 'none';
        div_phone_system.style.display = 'none';
        div_gestion_system.style.display = 'block';
      }
      // identificar_medio(call_data);

      document.getElementById("id_gestion").value = (gestion['id']);

      $("#id_contacto").val(gestion['contacto_pk']); // Le pego al modal el pk contacto
      $("#id_contacto_gestion").val(gestion['contacto_pk']); // Le pego al FORMULARIO GESTION el pk contacto
      $("#id_metadata").val(gestion['metadata']);
      $("#id_tipo").val(gestion['tipo']);
      actualizarResultado(respuesta['tipo'],respuesta['resultado'] );
      $("#id_medio").val(gestion['medio']);
      // $("#id_resultado").val(gestion['resultado']); lo pone la funcion
      $("#id_calificacion").val(gestion['calificacion']);
      $("#id_observaciones").val(gestion['observaciones']);


      // Entonces el div superior del CONTACTO lo actualizo
      contacto_pk = gestion['contacto_pk'];
      contacto_full_name = gestion['contacto_full_name'];
      imagen = gestion['contacto_imagen'];

      div_contacto_telefonia.setAttribute('contacto-pk', contacto_pk);

      const imagen_contacto = div_contacto_telefonia.getElementsByClassName('imagen-contacto')[0];
      if (imagen == '') {
        imagen_contacto.setAttribute('src', '/static/truecontact/truecontact/img/unknow-user.png');
      } else {
        imagen_contacto.setAttribute('src', imagen);
      }

      const nombre_contacto = div_contacto_telefonia.getElementsByClassName('nombre-contacto')[0];
      nombre_contacto.textContent = contacto_full_name;

      div_contacto_telefonia.setAttribute('data-calldata', JSON.stringify(call_data));

      //entonces pongo el pk del pedido en FORM_PEDIDO
      $("#id_gestioncontacto").val(gestion['id']); //coloco el pk dela gestion el form del pedido
      //VOY A COLSULTAR SI EXISTEUN PEDIDO CREADO y lo renderiso
      //consultar_pedido(gestion['id']);

    })
    .catch(function (error) {
      console.log(error);
    });
  btn_tab_gestion.click();



};

function crear_html_telefono(data) {
  let tr_telefono = document.createElement('tr');
  tr_telefono.setAttribute('id-dato-contacto', data['id']);
  tr_telefono.setAttribute('id', data['telefono']);

  let td_label = document.createElement('td');
  td_label.innerHTML = data['label'];

  let td_telefono = document.createElement('td');
  td_telefono.innerHTML = data['telefono'];

  let td_accion = document.createElement('td');
  td_accion.classList.add('text-center');
  td_accion.innerHTML= `
  <ul class="table-controls">
  <li><a href="javascript:void(0);"  onclick="$(this).editartelefono(this);" data-toggle="tooltip" data-placement="top" data-telefono="${data['telefono']}" data-contacto="${id_contacto.value}" title="Edit"><i class="flaticon-fill-tick text-primary fs-20" ></i></a></li>
  <li><a href="javascript:void(0);" onclick="$(this).eliminartelefono(this);" data-toggle="tooltip" data-placement="top" data-telefono="${data['telefono']}" data-contacto="${id_contacto.value}" title="Delete"><i class="flaticon-close-fill text-danger fs-20"></i></a></li>
</ul>
  `;

  tr_telefono.appendChild(td_label);
  tr_telefono.appendChild(td_telefono);
  tr_telefono.appendChild(td_accion);

  return tr_telefono;
}

function crear_html_email(data) {
  let tr_principal = document.createElement('tr');
  tr_principal.setAttribute('id-dato-contacto', data['id']);

  let td_label = document.createElement('td');
  td_label.innerHTML = data['label'];

  let td_email = document.createElement('td');
  td_email.innerHTML = data['email'];

  let td_accion = document.createElement('td');
  td_accion.classList.add('text-center');
  td_accion.innerHTML= `
    <ul class="table-controls">
      <li><a href="javascript:void(0);"  data-toggle="tooltip" data-placement="top" title="Edit"><i class="flaticon-fill-tick text-primary fs-20"></i></a></li>
      <li><a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete"><i class="flaticon-close-fill text-danger fs-20"></i></a></li>
    </ul>
  `;

  tr_principal.appendChild(td_label);
  tr_principal.appendChild(td_email);
  tr_principal.appendChild(td_accion);

  return tr_principal;
}

function crear_html_direccion(data) {
  let tr_principal = document.createElement('tr');
  tr_principal.setAttribute('id-dato-contacto', data['id']);

  let td_label = document.createElement('td');
  td_label.innerHTML = data['label'];

  let td_valor = document.createElement('td');
  td_valor.innerHTML = data['d1'] + ' ' + data['d2'] + ' ' + data['d3'];

  let td_accion = document.createElement('td');
  td_accion.classList.add('text-center');
  td_accion.innerHTML= `
    <ul class="table-controls">
      <li><a href="javascript:void(0);"  data-toggle="tooltip" data-placement="top" title="Edit"><i class="flaticon-fill-tick text-primary fs-20"></i></a></li>
      <li><a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Delete"><i class="flaticon-close-fill text-danger fs-20"></i></a></li>
    </ul>
  `;

  tr_principal.appendChild(td_label);
  tr_principal.appendChild(td_valor);
  tr_principal.appendChild(td_accion);

  return tr_principal;
}

$('#form_agregar_gestion').change(function () {                     //  METTODO AUTOMATICO DE GUARDA ///////
  var gestion = $('#id_gestion').val();
  console.log("formulario cambiando llamo a guardado automatico. GESTION: " + gestion);
  let url = Urls.tc_gestion_ajax_guardar();

  if (gestion != '') {
    console.log("Tiene una gestion asociada")
    $.post(url, $("#form_agregar_gestion").serialize(), function (data, status) {
      var respuesta = data['respuesta'];
      var valido = data['valido'];

      if (valido == true) {
        $("#id_estado").val(data['estado']);
        console.log("llego de guardar la gestion" + data['estado']);
      }
      else {
        console.log(respuesta);
      }
    });
  }
  else {
    console.log("No hay gestion asociada fallo al guardar contactosJS")

  }
});

function actualizar_gestion() {
  let gestion = document.getElementById('id_gestion').value;
  let url = Urls.tc_gestion_ajax_guardar();

  if (gestion != '') {
    console.log("Tiene una gestion asociada")
    $.post(url, $("#form_agregar_gestion").serialize(), function (data, status) {
      var respuesta = data['respuesta'];
      var valido = data['valido'];

      if (valido == true) {
        $("#id_estado").val(data['estado']);
        console.log("llego de guardar la gestion" + data['estado']);
      }
      else {
        console.log(respuesta);
      }
    });
  }
  else {
    console.log("No hay gestion asociada fallo al guardar contactosJS")

  }
}

var index = 1;

const btn_new_telefono = document.getElementById('btn-new-telefono');

btn_new_telefono.addEventListener('click', () => {
  let table_telefonos_main_body = document.querySelector('#contacto-telefonos tbody');
  contacto_pk = $('#contacto_pk').val();
  console.log("Agregarndo nuevo telefono al contacto: " + index)
  var tr_telefono = `
  <tr id="tr${index}">
      <td>
          <select name="label" required="" class="form-control bm-4" id="id_label${index}">
          <option value="" selected="">-----</option>
          <option value="principal">Principal</option>
          <option value="casa">Casa</option>
          <option value="trabajo">Trabajo</option>
          <option value="movil">Movil</option>
          <option value="estudio">Estudio</option>
          <option value="particular">Particular</option>
          <option value="otro">Otro</option>
          </select>
      </td>
      <td><input type="number" step="1" class="form-control" required="" id="id_numero${index}"></td>
      <td class="text-center">
          <a href="javascript:;" onclick="$(this).addContactoTelefono(this);" class="btn btn-outline-primary mb-4 mr-2" data-id="${index}">
          Añadir </a>
      </td>

  </tr>
  `;
  $("#body_telefono").append(tr_telefono);
  index++;
});

$.fn.addContactoTelefono = function(){
  const tr_id = $(this).data('id');
  const banner = document.querySelector('#contacto');
  var contacto_pk = JSON.parse(banner.dataset.calldata).id_contacto
  var label = $("#id_label"+tr_id).val();
  var numero = $("#id_numero"+tr_id).val();
  // let url = "{% url 'tc_contacto-telefono-add-ajax' %}";
  let url = Urls.tc_contacto_telefono_add_ajax()
  $('#tr'+tr_id).remove();

  $.get(url, {'contacto_pk': contacto_pk, 'label':label, 'numero':numero }, function(data, status){
      if (status == "success"){
          var contactodatos_pk = data['contactodatos_pk'];
          console.log("ok llego bien"+ contactodatos_pk);

          var tr_telefono = `
              <tr id="${numero}">
                  <td>${label}</td>
                  <td>${numero}</td>
                  <td class="text-center">
                  </td>

              </tr>
              `;
          $("#body_telefono").append(tr_telefono);
      }
  })
}


// ********  FUNCIONES  CUANDO ENTRA UNA LLAMADA Y EL NUMERO PERTENECE A VARIOS CONTACTOS  ********////////

// ********  Si la lista no tiene el contacto deseado abra el modal de CREAR NUEVO CONTACTO ASOCIADO A ESE MISMO NUMERO ********////////
const btn_nuevo_contacto = document.getElementById('btn-nuevo-contacto');

btn_nuevo_contacto.addEventListener('click',() =>{
  console.log("click on nuevo contacto")
  $("#modal-ver-contactos").modal('hide');
  $("#modal-contacto").modal('show');
})

//** si la lista escogen uno va y ubica al sujeto y actualiza la gestion */
$.fn.gestionContacto = function () {
  const id_contacto = $(this).data('contacto_id');
  const nombre_corto = $(this).data('nombre_corto');
  const nombre_largo = $(this).data('nombre_largo');

  console.log(" el pk del contacto escogido es" + id_contacto);
  console.log(" el nombre corto escogido es" + nombre_corto);
  console.log(" el nombre largo escogido es" + nombre_largo);


  let call_data = JSON.parse(div_contacto_telefonia.getAttribute('data-calldata'));
  call_data['id_contacto'] = id_contacto;
  div_contacto_telefonia.setAttribute('data-calldata', JSON.stringify(call_data));

  document.getElementById('id_contacto').value = call_data['id_contacto'];
  document.getElementById('id_contacto_gestion').value = call_data['id_contacto'];
  document.getElementById('id_metadata').value = JSON.stringify(call_data);
  actualizar_gestion();
  $("#modal-ver-contactos").modal('hide');

  let data_id = div_contacto_telefonia.getAttribute('data-id');
  let div_gestion = document.getElementById(`gestion-${data_id}`);

  const nombre_contacto = div_contacto_telefonia.getElementsByClassName('nombre-contacto')[0];
  const gestion_name = div_gestion.getElementsByClassName('nombre')[0];

  nombre_contacto.textContent = nombre_largo;
  gestion_name.textContent = nombre_corto;


}

/* FUNCION QUE DETECTA SI CAMBIO EL NO. DOCUMENTO Y BUSCA SI HAY CONTACTO ASOCIADO */
$('#id_documento').change(function () {
  // alert("cambio documento");
  var documento = $(this).val();
  let url = Urls.tc_buscar_documento_ajax();
  console.log("Fue el documento" + documento);
  $.get(url, {'tipo': 'consulta_cc', 'doc': documento }, function(data, status){
        if (status == "success"){
          nombre = data['nombres'];
          console.log("CONSULTA EXITOSA");
          if (nombre == ''){

            $('#id_contacto').val("");
            $("#id_contacto_gestion").val(""); // Le pego al FORMULARIO GESTION el pk contacto
            // $('#id_nombres').val("");
            // $('#id_apellidos').val("");
            // $('#id_email').val("");
            // $('#id_movil').val("");
            // $('#id_tipo_identificacion').val("")
            // $("#btn_guardar_contacto").show();
            // $("#btn-add-encuesta").hide();
            // $("#form_agregar_gestion").hide();
            // $("#body-gestiones-contacto").empty();
            console.log('nombre vacio');

          }
          else{
            let timerInterval
            Swal.fire({
              title: 'Buscando Contacto!',
              timer: 750,
              timerProgressBar: true,
              onBeforeOpen: () => {
                Swal.showLoading()
                timerInterval = setInterval(() => {
                  const content = Swal.getContent()
                  if (content) {
                    const b = content.querySelector('b')
                    if (b) {
                      b.textContent = Swal.getTimerLeft()
                    }
                  }
                }, 100)
              },
              onClose: () => {
                clearInterval(timerInterval)
              }
            }).then((result) => {
              /* Read more about handling dismissals below */
              // if (result.dismiss === Swal.DismissReason.timer) {
              //   console.log('I was closed by the timer')
              // }
            })
              // $('#id_contacto').val();
              let string_url = window.location.origin + Urls.tc_contacto_ajax_ver();
              let url = new URL(string_url),
              params = { 'pk_contacto': data["id_contacto"] };
              $('#id_contacto').val(data["id_contacto"]);
              Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
              fetch(url, {  method: 'get' })
              .then(function (res) { return res.json(); })
              .then(function (respuesta) {
                const contacto = respuesta['contacto'];
                document.getElementById('id_tipo_identificacion').value = contacto['tipo_identificacion'];
                document.getElementById('id_documento').value = contacto['documento'];
                document.getElementById('id_nombres').value = contacto['nombres'];
                document.getElementById('id_apellidos').value = contacto['apellidos'];
                $("#profile-image-upload").attr('src', contacto['imagen_url']);
                $("#id_industria").val(contacto['industria']); //Esto es de la libreria de select 2 toca con jquery
                $("#id_industria").trigger('change'); //Esto es de la libreria de select 2 toca con jquery
                document.getElementById('id_es_compania').checked = contacto['es_compania'];
                document.getElementById('id_bool_1').checked = contacto['es_cliente'];
                document.getElementById('id_bool_2').checked = contacto['es_proveedor'];
                document.getElementById('id_bool_3').checked = contacto['posible_cliente'];
                document.getElementById('id_observaciones_contacto').value = contacto['observaciones'];
                telefonos = contacto['telefonos'];
                emails = contacto['emails'];
                direcciones = contacto['direcciones'];

                telefonos.forEach((telefono) => {
                  let tr_telefono = crear_html_telefono(telefono);
                  table_telefonos_body.appendChild(tr_telefono);
                });

                emails.forEach((email) => {
                  let tr_email = crear_html_email(email);
                  table_emails_body.appendChild(tr_email);
                });

                direcciones.forEach((direccion) => {
                  let tr_direccion = crear_html_direccion(direccion);
                  table_direcciones_body.appendChild(tr_direccion);
                });

              })
              .catch(function (error) {
                console.log(error);
              });
          }
        }
      })
});

/* FUNCION QUE DETECTA SI CAMBIO EL Tipo Gestion y cambia los resultados */
$('#id_tipo').change(function () {
  var tipo_gestion = $(this).val();
  let url = Urls.resultados_ajax();
  console.log("Fue el tipo gestion" + tipo_gestion);
  $("#id_resultado").empty();
  $("#id_resultado").append("<option value=''>Seleccione</option>");
  if (tipo_gestion != ''){
    $.get(url, {"tipo": tipo_gestion }, function(data,status){
      if (status=="success"){
        var resultados = data['resultados'];
        for (i=0; i<resultados.length; i++){
          var resultado = resultados[i];
          $('#id_resultado').append("<option value='"+resultado["id"]+"'>"+resultado["nombre"]+"</option>").trigger("chosen:updated");
        }
      }
    });
  }

});



/* Funcion que actualiza el resultado si esta consultando gestion */

function actualizarResultado (id_tipo_gestion, id_resultado) {
  // console.log(">>>> actualizarResultado >> 4.Actualizar Resultado de form gestion  " + id_tipo_gestion);
  let url = Urls.resultados_ajax();
  $("#id_resultado").empty();
  $("#id_resultado").append("<option value=''>Seleccione</option>");
  if (id_tipo_gestion != ''){
    $.get(url, {"tipo": id_tipo_gestion }, function(data,status){
      if (status=="success"){
        var resultados = data['resultados'];
        for (i=0; i<resultados.length; i++){
          var resultado = resultados[i];
          $('#id_resultado').append("<option value='"+resultado["id"]+"'>"+resultado["nombre"]+"</option>").trigger("chosen:updated");
        }
        $("#id_resultado").val(id_resultado);
      }
    });
  }
}

function actualizarResultado1 (id_motivo_gestion, id_resultado) {
  // console.log(">>>> actualizarResultado >> 4.Actualizar Resultado de form gestion  " + id_tipo_gestion);
  let url = Urls.resultados_ajax();
  $("#selectresultados").empty();
  $("#selectresultados").append("<option value=''>---------</option>");
  if (id_motivo_gestion != ''){
    $.get(url, {"motivo": id_motivo_gestion }, function(data,status){
      if (status=="success"){
        var resultados = data['resultados'];
        for (i=0; i<resultados.length; i++){
          var resultado = resultados[i];
          $('#selectresultados').append("<option value='"+resultado["id"]+"'>"+resultado["nombre"]+"</option>").trigger("chosen:updated");
        }
        $("#selectresultados").val(id_resultado);
      }
    });
  }
}

// ********  FFUNCIONES RELACIONADAS AL DIRECTORIO DE CONTACTOS COMO: buscar numero debe ser único  ********////////
// document.getElementById('dataView').contentWindow.consultar_telefono(); //esta linea permite que el ChildIframe pueda llegar al metodo

function consultar_telefono (numero_telefono, tr_id){
  console.log("CONSULTANDO TELEFONO!", numero_telefono);
  let url = Urls.tc_validar_telefono_ajax();
  $.get(url, {"numero_telefono": numero_telefono }, function(data,status){
    if (status=="success"){
      var respuesta = data['respuesta'];
      console.log("resultados", respuesta )
      if (respuesta['existe'] === true ){
        console.log("YA EXISTE EL CONTACTO ASESOR!!!")
        var nombre_contacto = respuesta['contacto_full_name'];
        var asesor = respuesta['agente_full_name'];
        swal.fire({
          icon: 'error',
          title: 'Contacto ya existe: ' + nombre_contacto,
          text: 'Agente asesor: ' + asesor
        })
        console.log("tr id es..: " + tr_id);
        $("#dataView").contents().find(`#tr${tr_id}`).remove();

      }else{
        console.log("no existe todo bien.....");
      }


    }
  });
};

const consultar_telefono_directorio = async (numero_telefono, tr_id) =>{
  let string_url = window.location.origin + Urls.tc_consultar_telefono_directorio();
  let url = new URL(string_url), params = { 'numero_telefono': numero_telefono };
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

  let response =  await fetch(url, {method: 'GET'})
  let respuesta = await response.json()

  existe = respuesta['data']['existe']
  if(existe === true){
    swal.fire({
      icon: 'error',
      title: 'El teléfono ya está registrado',
      text: respuesta['message']
    })
    let tr_delete = document.getElementById(`tr_numero${tr_id}`)
    
    tr_delete.parentNode.removeChild(tr_delete)
  }
}

// ********    ********////////

btn_crearEncuesta = document.getElementById('btn-crearEncuesta');
btn_crearEncuesta.addEventListener('click',  function(){
  let url = '/tc/sanautos/crear/encuesta/';
  let form = $("#form_crear_encuesta");
  var pk_gestion = $("#gestion_encuesta").val();
  if ( pk_gestion != '' ){
    $.post(url, form.serialize(), function(data, status) {
      if(status == "success"){
        var data_encuesta = data['encuesta']
        if (data_encuesta != ''){
          Swal.fire("Nueva encuesta Registrada...");
          $("#modal-encuesta").modal('hide');
          recomendaria_reset = document.getElementById('recomendaria');
          recomendaria_reset.value = ''
          calificacion_reset = document.getElementById('calificacion');
          calificacion_reset.value = ''
          observaciones_encuesta_reset = document.getElementById('observaciones_encuesta');
          observaciones_encuesta_reset.value = ''

        }

      }
    });

  }
  else{
    console.log("No hay gestion");
  }
})

  $.fn.eliminartelefono = function () {
    const numero_telefono = $(this).data('telefono');
    const id_contacto = $(this).data('contacto');

    Swal.fire({
      title: 'Quiere eliminar el número ?',
      text: numero_telefono,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si'
    }).then((result) => {
      if (result.isConfirmed) {
        let url = Urls.tc_eliminar_telefono_ajax()

        $.get(url, {'id_contacto': id_contacto,'numero_telefono':numero_telefono }, function(data, status){
            if (status == "success"){
            
              Swal.fire(
                'Eliminado!',
                'Se eliminó correctamente el número.',
                'success'
              )
                var button_id = numero_telefono;
                  //cuando da click obtenemos el id del boton
                $('#' + button_id + '').remove(); //borra la fila
                  
                
            }
        })

        
      }
    })

  }

  $.fn.editartelefono = function () {
    const numero_telefono = $(this).data('telefono');
    const id_contacto = $(this).data('contacto');

   
    var button_id = numero_telefono;
    // $('#' + button_id + '').remove();
    tr_button = document.getElementById(button_id).innerText;
    posicion_t = tr_button.indexOf("\t")
    label_button = tr_button.slice(0,posicion_t)


    let table_telefonos_main_body = document.querySelector('#contacto-telefonos tbody');
    contacto_pk = $('#contacto_pk').val();
    console.log("Agregarndo nuevo telefono al contacto: " + button_id)
    var tr_telefono = `
    <tr id="tr${button_id}">
        <td>
            <select name="label" required="" class="form-control bm-4" id="id_label${button_id}">
            <option value="" selected="">----</option>
            <option value="whatsapp">Whatsapp</option>
            <option value="principal">Principal</option>
            <option value="casa">Casa</option>
            <option value="trabajo">Trabajo</option>
            <option value="movil">Movil</option>
            <option value="estudio">Estudio</option>
            <option value="particular">Particular</option>|
            <option value="otro">Otro</option>
            </select>
        </td>
        <td><input type="number" step="1" class="form-control" required="${button_id}" id="id_numero${button_id}"> </td>
        <td class="text-center">
            <a href="javascript:;" onclick="$(this).ModificarTelefono(this);" class="btn btn-outline-primary mb-4 mr-2" data-id="${button_id}">
            Modificar </a>
        </td>
  
    </tr>
    `;
    $('#' + button_id + '').replaceWith(tr_telefono);
    document.getElementById('id_numero'+button_id).value=button_id;
  }


  $.fn.ModificarTelefono = function () {
  const tr_id = $(this).data('id');
  const numero_telefono = tr_id;

  const banner = document.querySelector('#contacto');
  var contacto_pk = JSON.parse(banner.dataset.calldata).id_contacto
  var label = $("#id_label"+tr_id).val();
  var numero = $("#id_numero"+tr_id).val();
  
  let url = Urls.tc_editar_telefono_ajax()
  $('#tr'+tr_id).remove();

  $.get(url, {'contacto_pk': contacto_pk, 'label':label, 'numero':numero,'numero_telefono':numero_telefono }, function(data, status){
      if (status == "success"){
          var contactodatos_pk = data['contactodatos_pk'];
          console.log("ok llego bien"+ contactodatos_pk);

          var tr_telefono = `
              <tr id="${numero}">
                  <td>${label}</td>
                  <td>${numero}</td>
                  <td class="text-center">
                  </td>

              </tr>
              `;
          $("#body_telefono").append(tr_telefono);
      
      }
  })


  }

$('#selectmotivos').change(() => {
  select_motivo = document.getElementById('selectmotivos')
  var optionSelected = select_motivo.options[select_motivo.selectedIndex];
  let url =  Urls.tc_obtener_resultado_motivo();
  if (!optionSelected.value){
    $("#selectresultados").empty();
    $("#selectresultados").append("<option value selected>---------</option>");
  }
  $.get(url,  {"pk_motivo": optionSelected.value }, function(data, status) {
    if(status == "success"){
      // $("#selectresultados").append("<option value=''>Seleccione</option>");
      $("#selectresultados").empty();
      $("#selectresultados").append("<option value selected>---------</option>");
      for (var i = 0; i < data.length; i++) {
        $("#selectresultados").append("<option value='"+data[i].pk+"'>"+ data[i].nombre +'</option>');
      };
    }
    else {
      alert('Error en la consulta')
    }
  });
});