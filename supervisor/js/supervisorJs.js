
function verGestion(id_gestion){
    const url = Urls.tc_gestion(id_gestion);
    $.get(url, {}, function(data, status){
      if (status == 'success'){
        console.log("gestion")
        console.log(data);
        const gestion = data['gestion'];
        console.log('<<>><><><><><>>')
        console.log(gestion['motivo'])
        $("#info-gestion").text(`GESTION ${gestion['id']} - ${gestion['contacto']['nombre_corto']}`);
        $("#info-fecha").text(gestion['fecha_gestion']);
        $("#info-agente").text(gestion['agente']);
        $("#info-tipo").text(gestion['tipo_nombre']);
        $("#info-medio").text(gestion['medio']);
        if (gestion['estado'] == 'pendiente') {
          $("#info-estado").html(`<span class="badge bg-warning">${gestion['estado']}</span>`);
          }else if (gestion['estado'] == 'en_proceso') {
            $("#info-estado").html(`<span class="badge bg-primary">${gestion['estado']}</span>`);
          }else if (gestion['estado'] == 'finalizado') {
            $("#info-estado").html(`<span class="badge bg-success">${gestion['estado']}</span>`);
          }else{
            $("#info-estado").html(`<span class="badge bg-danger">Error</span>`);
          }
        
        let tyc = gestion['tyc'] == false ? 'No':'Sí';
        $("#info-motivo").text(gestion['motivo']);
        $("#info-resultado").text(gestion['resul']);
        $("#info-especialidad").text(gestion['especialidad']);
        $("#info-entidad").text(gestion['entidad']);
        $("#info-tipo-consulta").text(gestion['tipo_consulta']);
        $("#info-tyc").text(tyc);
        $("#info-observaciones").text(gestion['observaciones']);
        //meti un nuevo if que pregunta en el HTL/modal si es un supervisor

          if (gestion['medio'] == 'telefonia') {
            $("#info-registro").html(`
              <audio controls="">
                <source src="${gestion['url_grabacion']}" type="audio/mpeg">
              </audio>
            `);
          }else if(gestion['medio'] == 'whatsapp'){
            const info_registro = document.getElementById('info-registro');
            info_registro.textContent = '';
            const ver_mensajes = document.createElement('a');
            ver_mensajes.textContent = 'Ver mensajes Whatsapp';
            ver_mensajes.setAttribute('data-id', id_gestion);
            ver_mensajes.setAttribute('data-calldata', JSON.stringify(gestion['call_data']));
            ver_mensajes.setAttribute('href', 'javascript:;');
            // console.log('info registro: ', info_registro );
            // console.log('a de registros: ', ver_mensajes );
            ver_mensajes.addEventListener('click', ver_registro_whatsapp);
            info_registro.appendChild(ver_mensajes);
          }else if(gestion['medio'] == 'sms'){
            $("#info-registro").html(`
              <p">
                <b>Mensaje de texto: </b> ${gestion['sms']}
              </p>
            `);
          }
          else{
            $("#info-registro").html(`
              <p">
                Gestion manual
              </p>
            `);
          }


        const registro_mensajes = document.getElementById('registro_mensajes');
        registro_mensajes.style.display = 'none';
        $("#modal-ver-gestion").modal('show');
      }else{
        toastr.error(`Error al consultar la gestion ${id_gestion}, por favor contacte con su administrador`);
      }
    });
  }
async function verGestionConmutador(id_gestion){
  let string_url = window.location.origin + Urls.tc_cargar_gestion_conmutador()
  let url = new URL(string_url), params = {'id_gestion':id_gestion}
  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))

  let response =  await fetch(url, {method: 'GET'})
  let respuesta = await response.json()
  data = respuesta['respuesta']

  document.getElementById('gestion-info').textContent = `Gestión ${data['id']} - ${data['directorio_full_name']}`
  document.getElementById('fecha-info').textContent = data['fecha_gestion']
  document.getElementById('agente-info').textContent = data['agente']
  document.getElementById('tipo-info').textContent = data['tipo_nombre']
  document.getElementById('medio-info').textContent = data['medio_display']
  document.getElementById('estado-info').textContent = data['estado']
  if(data['estado'] == "Finalizada"){
    document.getElementById('estado-info').style.color = "#67BA19"
  } else {
    document.getElementById('estado-info').style.color = "#CF9B1B"
  }
  document.getElementById('nombre-info').textContent = data['nombre']
  document.getElementById('directorio-info').textContent = data['directorio_destino_nombre']
  document.getElementById('telefono-info').textContent = data['telefono_numero']
  document.getElementById('resultado-info').textContent = data['resultado_display']
  document.getElementById('observaciones-info').textContent = data['observaciones']

  if (data['medio_display'] == 'Telefonía') {
    document.getElementById('registro-info').innerHTML = `
      <audio controls>
          <source src="${data['url_grabacion']}" type="audio/mpeg">
      </audio>
    `
  }
  let historial = data['historial'];

  if (historial.length > 0){
    document.getElementById("tbody_historial_conmutador").innerHTML = '';
    historial.forEach(function (item, index) {
    //  console.log(item, index);
      var tr_historial = `
        <tr id="tr${index}" >
          <td onClick="verGestionConmutador(${item['id']})" >
            <h6 class="mb-0">${item['medio']}</h6>
            <p><small>${item['fecha_creacion']}</small></p>
            <h6 class="mb-0">Estado: ${item['estado']}</h6>
          </td>
        </tr>
        `
        parent.document.getElementById("tbody_historial_conmutador").innerHTML += tr_historial
    })
  }
  else{
    document.getElementById("tbody_historial_conmutador").innerHTML = '';
  }
  $("#modal-ver-gestion-conmutador").modal('show')
}
function ver_registro_whatsapp(e) {
  const registro_mensajes = document.getElementById('registro_mensajes');
  registro_mensajes.textContent = '';
  registro_mensajes.style.display = 'block';
  let id_gestion = e.target.getAttribute('data-id');
  let call_data_json = e.target.getAttribute('data-calldata');
  let call_data = JSON.parse(call_data_json);
  console.log(call_data);
  let dialogo_id = call_data.chat_id;
  let mensaje_inicial = call_data.mensaje_inicial;
  let mensaje_final = call_data.mensaje_final;
  if (mensaje_final == null) {
    mensaje_final = '';
  }
  console.log(mensaje_final);
  let url = `/tc/dialogo/${dialogo_id}/mensajes/`;
  url = url + `?mensaje_inicial=${mensaje_inicial}&mensaje_final=${mensaje_final}`;
  fetch(url)
    .then(function(res) {
      return res.json();
    })
    .then(function(respuesta) {
      let mensajes_list = respuesta['mensajes'];
      for (i = 0; i < mensajes_list.length; i++) {
        const mensaje = mensajes_list[i];
        const mensaje_container = crear_html_mensaje(mensaje);
        registro_mensajes.appendChild(mensaje_container);
      }
    })
    .catch(function(error) {
      console.log(error);
    });
}
