// Elementos obtenenidos del modal llamar contaco en Modals
const input_agente = document.getElementById('idagt');

function llamar_contacto(id_campana, tipo_campana, id_contacto, telefono) {
    let url = Urls.tc_agente_llamar_contacto()

    let id_agente = input_agente.value;

    let form_llamar_contacto = new FormData();
    let csrf_token = obtener_cookie('csrftoken');

    form_llamar_contacto.append('csrfmiddlewaretoken', csrf_token);
    form_llamar_contacto.append('pk_agente', id_agente);
    form_llamar_contacto.append('click2call_type', 'contactos');
    form_llamar_contacto.append('pk_contacto', id_contacto);
    form_llamar_contacto.append('pk_campana', id_campana);
    form_llamar_contacto.append('tipo_campana', tipo_campana);
    form_llamar_contacto.append('telefono', telefono);

    console.log("llamando con campana, %s del tipo %s contacto %s telefono %s  ",id_campana , tipo_campana , id_contacto , telefono);
    
    fetch(url, {
      method: 'post',
      body: form_llamar_contacto,
    })
    .then( (response) => {
      console.log(`Response ${response}`);
    }).catch( (error) => {
      console.log(`Error ${error}`);
    });

}
