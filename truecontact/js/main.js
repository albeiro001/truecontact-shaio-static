// Obteniendo objetos

const form_filter = document.getElementById('form-filter');

// event Listeners
const btn_display_list = document.getElementById('display_list').addEventListener('click', function() {
    setDisplayValue('list');
});

const bth_display_kanban = document.getElementById('display_kanban').addEventListener('click', function() {
    setDisplayValue('kanban');
});

// functions
function setDisplayValue(display) {
    const display_value = document.getElementById('display_value');
    display_value.value = display;
    form_filter.submit();
};
