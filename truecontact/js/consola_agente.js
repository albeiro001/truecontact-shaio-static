/* ELEMENTOS */
const main_gestion = document.getElementById('main-gestion');
const main_crm = document.getElementById('main-crm');
const gestion_system = document.getElementById('gestion-system');
const div_phone_system = document.getElementById('phone-system');
const div_chat_system = document.getElementById('chat-system');

const div_main_right_col = document.getElementById('main-right-col');
const btn_show_right_col = document.querySelector('.show-main-right-col');
const btn_close_main_col_3 = document.querySelector('.close-main-col-3');
const btn_gestiones = document.getElementById('btn-gestiones');
const btn_leads = document.getElementById('btn-leads');
const btn_catalogo = document.getElementById('btn-catalogo');

const btn_ver_form_gestion = document.getElementById('btn-ver-form-gestion');
const btn_guardar_gestion = document.getElementById('guardar-gestion');
const btn_guardar_gestion_conmutador = document.getElementById('guardar_gestion_conmutadores')
const btn_guardar_rellamar = document.getElementById('guardar-rellamar');


const img_form_contacto = document.getElementById('profile-image-upload');
const input_img_contacto = document.getElementById('id_imagen');

const audio_notificacion = document.getElementById('audio_notificacion');
const modelo_origen_gestion = document.getElementById('modelo_origen_gestion');

const campana_nombre =  document.getElementById('nombre_campana')

/* MODAL */

$('#btn-ver-form-guion').on('click', function() {
  const url = "/tc/ajustes/ayuda/consulta/ajax/";
  $.get(url, function(data,status){
    var ayudas = data["ayudas"];
    console.log(`ayudas: ${ayudas}`);
    var div_ayudas = document.getElementById("hd-ayudas");
    div_ayudas.textContent = '';

    ayudas.forEach( (ayuda_detalle) => {

      let div_ayuda = document.createElement('div');
      div_ayuda.classList.add('card');
      div_ayuda.innerHTML = `
        <div class="card-header" style="background-color: #c2d5ff">
          <div class="mb-0">
            <div class="" data-toggle="collapse" data-target="#collapse-ayuda-${ayuda_detalle['id']}" aria-expanded="true" aria-controls="collapse-ayuda-${ayuda_detalle['id']}"
              style="font-size: 20px; color: black">
              ${ayuda_detalle['titulo']}
            </div>
          </div>
        </div>

        <div id="collapse-ayuda-${ayuda_detalle['id']}" class="collapse" aria-labelledby="ayuda-${ayuda_detalle['id']}" data-parent="#hd-ayudas">
          <div class="card-body">
            <p style="font-size: 16px;">${ayuda_detalle['descripcion']}</p>
          </div>
        </div>
      `;

      div_ayudas.appendChild(div_ayuda);

    });

    $('#modal-guion2').modal('show');
  })
});

$('#btn-ver-form-guion2').on('click', function() {
  const url = "/tc/ajustes/guion/consulta/ajax/";
  $.get(url, function(data,status){
    var guiones = data["guiones"];
    console.log(`guiones: ${guiones}`);
    var tabla_guiones = document.querySelector("#modal-guion-detail");
    tabla_guiones.textContent = '';

    guiones.forEach( (guion_detalle) => {
      console.log(guion_detalle['titulo']);
      $("#modal-guion-detail").append(`
        <h5><b>${guion_detalle['titulo']}</b></h5>
        <ul>
          <li>${guion_detalle['descripcion']}</li>
        </ul>

      `);
    });
    $('#modal-guion-saliente').modal('show');
  })

});




/* EVENTOS */
document.addEventListener('DOMContentLoaded', () => {

  let btns_menu_crm = document.querySelectorAll('.btn-crm');
  btns_menu_crm.forEach((elemento) => {
    elemento.addEventListener('click', () => {
      main_gestion.style.display = 'none';
      main_crm.style.display = 'block';
    });
  });

  if (!("Notification" in window)) {
    alert("Este navegador no soporta las notificaciones del sistema");
  }

  // Comprobamos si ya nos habían dado permiso
  else if (Notification.permission === "granted") {
    // Si esta correcto lanzamos la notificación
  }

  // Si no, tendremos que pedir permiso al usuario
  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
      // Si el usuario acepta, lanzamos la notificación
      if (permission === "granted") {
        var notification = new Notification("Notificaciones de Sistema Activadas!");
      }
    });
  }

});

btn_show_right_col.addEventListener('click', () => {
  div_main_right_col.classList.toggle('active');
});

btn_ver_form_gestion.addEventListener('click', () => {
  gestion_system.classList.toggle('active');
  btn_guardar_gestion.classList.toggle('active');
  btn_guardar_rellamar.classList.toggle('active');
  contacto =document.getElementById('contacto')
  id_chat=contacto.getAttribute('data-id')
  $('#id_calificacion').empty();
  $('#id_calificacion').append("<option value=''>Seleccione</option>").trigger("chosen:updated");
  const url = Urls.tc_contacto_calificacion_campana();
      $.get(url, {'id_gestion':id_gestion.value, 'id_chat':id_chat}, function(data, status) {

        if (status == "success") {
          calificaciones = data['calificaciones']
          for (i=0; i<calificaciones.length; i++){
            var resultado = calificaciones[i];
            $('#id_calificacion').append("<option value='"+resultado["id_calificacion"]+"'>"+resultado["nombre_calificacion"]+"</option>").trigger("chosen:updated");
          }

        }

      });
});

btn_gestiones.addEventListener('click', () => {
  document.querySelector('#btn-gestiones .info-gestiones').classList.toggle('active');
  document.getElementById('main-left-col').classList.toggle('active');
});

btn_catalogo.addEventListener('click', () => {
  document.getElementById('main-right-col').classList.toggle('active');
});


img_form_contacto.addEventListener('click', () => {
  console.log('Click img contacto!');
  input_img_contacto.click();
});

input_img_contacto.addEventListener('change', (e) => {
  fasterPreview(e.target);
});



// imagen profile contacto



/* FUNCIONES */
function ver_crm(url= Urls.tc_contacto()) {
  document.getElementById('dataView').src = url;
  main_gestion.style.display = 'none';
  main_crm.style.display = 'block';
}

function ver_gestion() {
  main_gestion.style.display = 'block';
  main_crm.style.display = 'none';
}


function identificar_medio(call_data){
  main_gestion.style.display = 'block';
  main_crm.style.display = 'none';
  btn_tab_gestion.classList.value = 'nav-link active';
  btn_tab_aux.classList.value = 'nav-link';
  tab_gestion.classList.value = 'tab-pane fade show active';
  tab_aux.classList.value = 'tab-pane fade';

  let medio = call_data['medio'];

  switch (medio) {
    case 'whatsapp':
      div_chat_system.style.display = 'block';
      div_phone_system.style.display = 'none';

      if (gestion_system.classList.contains('active') == true) {
        gestion_system.classList.remove('active');
      }
      if (btn_guardar_gestion.classList.contains('active') == true) {
        btn_guardar_gestion.classList.remove('active');
      }
      if (btn_guardar_rellamar.classList.contains('active') == false) {
        btn_guardar_rellamar.classList.add('active');
      }
      break;

    case 'telefonia':
        div_chat_system.style.display = 'none';
        div_phone_system.style.display = 'none';

        if (gestion_system.classList.contains('active') == false) {
          gestion_system.classList.add('active');
        }
        if(campana_nombre.value != "Conmutador"){
          if (btn_guardar_gestion.classList.contains('active') == false) {
            btn_guardar_gestion.classList.add('active');
          }
        } else {
          if (btn_guardar_gestion_conmutador.classList.contains('active') == false) {
            btn_guardar_gestion_conmutador.classList.add('active');
          }
        }
        if (btn_guardar_rellamar.classList.contains('active') == false) {
          btn_guardar_rellamar.classList.add('active');
        }

      break;

    case 'facebook':
      div_chat_system.style.display = 'block';
      div_phone_system.style.display = 'none'
      break;

    default:
      div_chat_system.style.display = 'block';
      div_phone_system.style.display = 'none';
  }
}

function crear_html_gestion(data) {
  console.log("CREANDO HTML GESTION CON DATA: ", data);
  /*
  let data = {
    'id': '3173837131@cs',
    'estado': 'nuevo',
    'medio': 'telefonia', // whatsapp, telefonia, facebook
    'nombre_corto': 'William Moreno',
    'mensajes_nuevos': '5',
    'imagen': '',
    'fecha_modificacion': 'Nov. 3, 2020, 9:31 AM'
    'nombre_campana': 'VENTAS',
  }

  */
  const div_gestion = document.createElement('div');
  div_gestion.classList.add('gestion');
  div_gestion.id = `gestion-${data['id']}`; // Le atribuye el id de la llamada
  div_gestion.setAttribute('data-id', data['id']);
  div_gestion.setAttribute('data-estado', data['estado']);
  var dic_modelo = {'origen': 'None', 'pk': ''}
  var dic_nuevo = JSON.stringify(dic_modelo);
  console.log('este es el cuando es nuevo ', modelo_origen_gestion)
  console.log('este es el valor  ', modelo_origen_gestion.value)
  if (modelo_origen_gestion.value == ''){
    div_gestion.setAttribute('data-origen-gestion',dic_nuevo)
  }
  else{
    div_gestion.setAttribute('data-origen-gestion',modelo_origen_gestion.value );
  }

  const gestion_row = document.createElement('div');
  gestion_row.classList.add('row');

  div_gestion.appendChild(gestion_row);

  const row_col3 = document.createElement('div');
  row_col3.classList.add('col-sm-3');

  gestion_row.appendChild(row_col3);

  const img_user = document.createElement('img');
  img_user.classList.add('img-user-dialogo');

  if (data['imagen'] == '') {
    img_user.src = '/static/truecontact/truecontact/img/unknow-user.png';
  } else {
    img_user.src = data['imagen'];
  }

  row_col3.appendChild(img_user);

  const div_img_platform = document.createElement('div');
  div_img_platform.classList.add('img-logo-platform');

  row_col3.appendChild(div_img_platform);

  const img_platform = document.createElement('img');
  img_platform.classList.add('logo-platforms');

  div_img_platform.appendChild(img_platform)

  const row_col9 = document.createElement('div');
  row_col9.classList.add('col-sm-9');

  gestion_row.appendChild(row_col9);

  const row_col9_row = document.createElement('div');
  row_col9_row.classList.add('row');

  row_col9.appendChild(row_col9_row)

  const div_nombre = document.createElement('div');
  div_nombre.classList.add('col-sm-12');

  row_col9_row.appendChild(div_nombre)

  const nombre = document.createElement('span');
  nombre.classList.add('nombre');
  nombre.textContent = data['nombre_corto'];

  div_nombre.appendChild(nombre);

  const span_cant_mensajes = document.createElement('span');
  span_cant_mensajes.classList.add('badge', 'badge-pill', 'badge-primary', 'float-right');
  span_cant_mensajes.textContent = data['mensajes_nuevos'];
  span_cant_mensajes.style.display = 'none';

  if (data['mensajes_nuevos'] > 0) {
    span_cant_mensajes.style.display = 'block';
  }

  div_nombre.appendChild(span_cant_mensajes);

  const nombre_campana = document.createElement('div');
  nombre_campana.classList.add('campana-text');
  row_col9.appendChild(nombre_campana);
  
  const span_nombre_campana = document.createElement('span');
  span_nombre_campana.textContent = data['nombre_campana'];
  nombre_campana.appendChild(span_nombre_campana);


  const div_fecha = document.createElement('div');
  div_fecha.classList.add('time', 'text-right');

  row_col9.appendChild(div_fecha);

  const span_fecha = document.createElement('span');
  span_fecha.textContent = data['fecha_modificacion'];

  div_fecha.appendChild(span_fecha);

  switch (data['medio']) {
    case 'whatsapp':
      div_gestion.classList.add('whatsapp');
      div_gestion.addEventListener('click', (e) => {
        cargar_mensajes_dialogo(data['id']);
        select_this_box(data['id']);
        

      });
      img_platform.src = '/static/truecontact/truecontact/img/logo-whatsapp.png';
      break;

    case 'facebook':
      div_gestion.classList.add('facebook');
      div_gestion.addEventListener('click', (e) => {
        console.log('Cargar mensajes de Facebook');
      });
      img_platform.src = '/static/truecontact/truecontact/img/logo-facebook.svg';
      break;

    case 'telefonia':
      div_gestion.classList.add('phone');
      img_platform.src = '/static/truecontact/truecontact/img/logo-phone2.svg';
      div_gestion.addEventListener('click', (e) => {
        if(campana_nombre.value != "Conmutador"){
          cargar_gestion_contacto(data['id_gestion']);
        } else {
          cargar_gestion_conmutador_contacto(data['id_gestion'])
        }
        select_this_box(data['id']);
      });
      div_gestion.setAttribute('data-gestion', data['id_gestion']);
      break;

    default:
      div_gestion.classList.add('whatsapp');
      img_platform.src = '/static/truecontact/truecontact/img/logo-whatsapp.png';
  }

  return div_gestion;
}

function crear_html_contacto(datos_contacto) {

  let card_top_seccion = document.createElement('div');
  card_top_seccion.classList.value = 'card-top-section text-right';
  card_top_seccion.innerHTML = `
    <div class="dropdown">
      <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="flaticon-dots"></i>
      </a>
      <div class="dropdown-menu" aria-labelledby="dropdownMenuLink5">
        <a class="dropdown-item" href="javascript:void(0);">Ver Pedidos</a>
        <a class="dropdown-item" href="javascript:void(0);">Ver Facturas</a>
        <a class="dropdown-item" href="javascript:void(0);">Seguir</a>
      </div>
    </div>
  `;

  let card_middle_seccion = document.createElement('div');
  card_middle_seccion.classList.value = 'card-mid-section mb-3';
  card_middle_seccion.innerHTML = `
    <div class="usr-profile mx-auto"> <!-- div contacto -->
        <span class="badge badge-primary">5</span> <!-- span gestiones contacto -->
        <img alt="admin-profile" src="static/truecontact/template/assets/img/90x90.jpg" class="rounded-circle"> <!-- img contacto -->
    </div>
    <div class="team-content mb-2"> <!-- div info contacto -->
        <h4 class="mt-2 mb-0"> William Moreno</h4> <!-- nombre contacto -->
        <span>Ingeniero de Proyectos</span> <!-- cargo contacto -->
        <p class="mt-2 mb-3">william.a.moreno.g@gmail.com</p> <!-- email contacto  -->
        <button class="btn btn-gradient-warning btn-rounded mb-4">Ver Perfil</button> <!-- btn ver perfil -->
    </div>
  `;


  let card_bottom_seccion = document.createElement('div');
  card_bottom_seccion.classList.value = 'card-bottom-section';
  card_bottom_seccion.innerHTML = `
    <div class="accion-buttons">
        <ul class="list-inline">
            <li class="list-inline-item">
                <i class="action-button-behance fas fa-phone"></i>
            </li>
            <li class="list-inline-item">
                <i class="action-button-dribbble fab fa-whatsapp"></i>
            </li>
            <li class="list-inline-item">
                <i class="action-button-facebook fab fa-facebook-f"></i>
            </li>
        </ul>
    </div>
  `;

  let card_container = document.createElement('div');
  card_container.classList.value = 'contact-1 text-center h-100';
  card_container.appendChild(card_top_seccion);
  card_container.appendChild(card_middle_seccion);
  card_container.appendChild(card_bottom_seccion);

  let card = document.createElement('div');
  card.classList.value = 'col-xl-3 col-lg-4 col-md-4 col-sm-6 mb-4';
  card.appendChild(card_container);

  return card;
}


function obtener_cookie(name) {
    let cookie_value = null;
    if (document.cookie && document.cookie !== '') {
        let cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            let cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookie_value = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookie_value;
}

function mostrar_notificacion(texto) {
  if (!("Notification" in window)) {
    alert("Este navegador no soporta las notificaciones del sistema");
  }

  // Comprobamos si ya nos habían dado permiso
  else if (Notification.permission === "granted") {
    // Si esta correcto lanzamos la notificación
    let notification = new Notification(texto);
    audio_notificacion.play();
  }
}

function fasterPreview( uploader ) {
  console.log('uploader', uploader);
  if ( uploader.files && uploader.files[0] ){
    console.log('set attribute src img')
    img_form_contacto.setAttribute('src', window.URL.createObjectURL(uploader.files[0]));
  }
}


function select_this_box(id_box){ //Funcion para pintar la cajita en donde estoy gestionando
  let gestion_box = 'gestion-'+ id_box;
  // alert("call select_this_box " + gestion_box);
  let elementos_gestion = document.querySelectorAll('#main-left-col .whatsapp');
  elementos_gestion.forEach((elemento) => {
    elemento.style.backgroundColor = "";
  });
  let llamadas_gestion = document.querySelectorAll('#main-left-col .phone');
  llamadas_gestion.forEach((elemento) => {
    elemento.style.backgroundColor = "";
  });
  let caja = document.getElementById(gestion_box);
  caja.style.backgroundColor = "#d3d3d3";
}