/*
*******************************************************************
* Elementos
*******************************************************************
*/
// const url_server = document.getElementById('kamailiohost').value;'tc.des.logytec.com.co'
const url_server = 'tc.des.logytec.com.co';
const url_chatapi_server = `wss://${url_server}:9090/ws`;
const id_agente = document.getElementById('idagt').value;
const username_agente = document.getElementById('agt-username').value;
let socket_agente = '';

const var_global = document.getElementById('modelo_origen_gestion').value; // VARIABLE DESDE EL HTML PARA QUE NO SE TRABE

const btn_tab_gestion = document.getElementById('btn-tab-gestion');
const tab_gestion = document.getElementById('tab-gestion');
const btn_tab_aux = document.getElementById('btn-tab-aux');
const tab_aux = document.getElementById('tab-aux');

const ventana_mensajes = document.getElementById('ventana-mensajes');

const gestiones_agente = document.querySelector('#gestiones-agente .container');
const gestiones_cola = document.querySelector('#gestiones-cola .container');

const div_contacto = document.getElementById('contacto');
const cantidad_dialogos_gestion = document.querySelector('#cantidad-gestiones-agente');
const cantidad_dialogos_nuevos = document.querySelector('#cantidad-gestiones-cola');
const cantidad_dialogos_gestion_2 = document.querySelector('#cantidad-gestiones-agente_2');
const cantidad_dialogos_nuevos_2 = document.querySelector('#cantidad-gestiones-cola_2');
const nuevo_mensaje = document.getElementById('input-mensajes');

/* end migrar a notificaciones*/

const div_adjuntar_menu = document.querySelector('.btn-group-fab');
const btn_adjuntar = document.getElementById('btn-adjuntar');
const btn_adjuntar_imagen = document.getElementById('btn-adjuntar-imagen');
const btn_adjuntar_archivo = document.getElementById('btn-adjuntar-archivo');
const input_imagen = document.getElementById('adjuntar-imagen');
const input_archivo = document.getElementById('adjuntar-archivo');

const btn_enviar_mensaje = document.getElementById('icono-enviar-mensaje');
const btn_enviar_audio = document.getElementById('icono-enviar-audio');

const div_audio = document.querySelector('.grabar-audio');
const btn_confirmar_audio = document.getElementById('confirmar-audio');
const btn_cancelar_audio = document.getElementById('cancelar-audio');
const span_duracion = document.getElementById('duracion');

const select_transferir_gestion_agente = document.getElementById('select-transferir-gestion-agente');
const btn_transferir_gestion_agente = document.getElementById('btn-transferir-gestion-agente');

const select_transferir_gestion_campana = document.getElementById('select-transferir-gestion-campana');
const btn_transferir_gestion_campana = document.getElementById('btn-transferir-gestion-campana');

const btn_nuevo_whatsapp = document.getElementById('btn-new-whatsapp');
const btn_whastapp_validar_mensaje = document.getElementById('whatsapp-validar-mensaje');
const div_nuevo_whatsapp = document.getElementById('new-whatsapp');
const swal_fire_transferir = document.getElementById('swal_fire_transferir');

const imagen_papele = document.getElementById('id_ctrlv')

/*
*******************************************************************
* Clases
*******************************************************************
*/

class InterfazWhatsapp {

  constructor(cantidad_gestion, cantidad_nuevos) {
    this.cantidad_gestion = cantidad_gestion;
    this.cantidad_nuevos = cantidad_nuevos;
    this.dialogo_mensajes = false; // me permite conocer si la ventana de mensajes esta activa

  }

  actualizar_cantidad_dialogos() {
    cantidad_dialogos_gestion.textContent = this.cantidad_gestion;
    cantidad_dialogos_nuevos.textContent = this.cantidad_nuevos;
  }

  aumentar_dialogos_gestion(cantidad) {
    this.cantidad_gestion += cantidad;
    this.actualizar_cantidad_dialogos();
  }

  disminuir_dialogos_gestion(cantidad) {
    if (this.cantidad_gestion > 0) {
      this.cantidad_gestion -= cantidad;
    }
    this.actualizar_cantidad_dialogos();
  }

  aumentar_dialogos_nuevos(cantidad) {
    this.cantidad_nuevos += cantidad;
    this.actualizar_cantidad_dialogos();
  }

  disminuir_dialogos_nuevos(cantidad) {
    if (this.cantidad_nuevos > 0) {
      this.cantidad_nuevos -= cantidad;
    }
    this.actualizar_cantidad_dialogos();
  }

  agregar_dialogo_gestion(dialogo) {
    const first_child = gestiones_agente.firstElementChild;
    gestiones_agente.insertBefore(dialogo, first_child);
    this.aumentar_dialogos_gestion(1);
    this.actualizar_cantidad_dialogos();
  }

  remover_dialogo_gestion(dialogo) {
    gestiones_agente.removeChild(dialogo);
    this.disminuir_dialogos_gestion(1);
    this.actualizar_cantidad_dialogos();
  }

  agregar_dialogo_nuevos(dialogo) {
    const first_child = gestiones_cola.firstElementChild;
    gestiones_cola.insertBefore(dialogo, first_child);
    this.aumentar_dialogos_nuevos(1);
    this.actualizar_cantidad_dialogos();
  }

  remover_dialogo_nuevos(dialogo) {
    gestiones_cola.removeChild(dialogo);
    this.disminuir_dialogos_nuevos(1);
    this.actualizar_cantidad_dialogos();
  }

  gestionar_dialogo(dialogo, dialogo_info) {
    let estado_dialogo = dialogo.getAttribute('data-estado');
    if (estado_dialogo === 'nuevo') {
      dialogo.setAttribute('data-estado', 'en_gestion');
      this.remover_dialogo_nuevos(dialogo);
      this.agregar_dialogo_gestion(dialogo);
      // Actualizo a los demas cliet-socket para que eliminen el dialogo de nuevos
      const datos = {
        'accion': 'cambiar_estado',
        'id_dialogo': dialogo_info.id
      }
      websocket_enviar_datos(datos);//esto es para decirle al websocket que tome el diialogo
    }

    const span_cant_mensajes = dialogo.getElementsByClassName('badge')[0];
    span_cant_mensajes.textContent = '0';
    span_cant_mensajes.style.display = 'none';

    const imagen_contacto = div_contacto.getElementsByClassName('imagen-contacto')[0];
    const nombre_contacto = div_contacto.getElementsByClassName('nombre-contacto')[0];
    const industrias_contacto = div_contacto.getElementsByClassName('industrias-text')[0];
    const campana_gestion = div_contacto.getElementsByClassName('campana-gestion')[0];



    if (dialogo_info['imagen'] == '') {
      imagen_contacto.src = '/static/truecontact/truecontact/img/unknow-user.png';
    } else {
      imagen_contacto.src = dialogo_info['imagen'];
    }

    nombre_contacto.textContent = dialogo_info['nombre_largo'];
    industrias_contacto.textContent = dialogo_info['industrias_contacto'];

  }

  cargar_mensajes_dialogo(mensajes, limpiar = false) {
    if (limpiar == true) {
      $("#overlay").fadeIn(300);
      ventana_mensajes.textContent = '';
    }
    $("#overlay").fadeOut(300);
    
    mensajes.forEach(function (mensaje) {
      const mensaje_container = crear_html_mensaje(mensaje);
      ventana_mensajes.appendChild(mensaje_container);
      ventana_mensajes.scrollTo(0, ventana_mensajes.scrollHeight);
    });
  }

  /*    ESTA FUNCION SE EJECUTA PARA METER LOS DIALOGOS NUEVOS QUE LLEGAN  */
  agregar_dialogos(data) { //data ES LA VARIABLE DONDE LLEGA UN JSON.DUMPS
    // console.log(">>>------ agregar_dialogos: ", dialogo_data);
    const dialogos_gestion = data['dialogos_en_gestion'];
    const dialogos_nuevos = data['dialogos_nuevos'];
    // var notificar = false;
    var mensaje = '';

    dialogos_gestion.forEach((dialogo_data) => {
      const id_agente_dialogo = dialogo_data['id_agente'];
      if (id_agente == id_agente_dialogo) {
        const dialogo = document.getElementById(`gestion-${dialogo_data['id']}`);
        // alert("mensjae nuevo");

        if (dialogo === null) {
          document.getElementById('nuevodialogotag').play();
          showNotification(mensaje="Nuevo Chat de: " + dialogo_data['nombre_largo'] );
          this.aumentar_dialogos_gestion(1);
          this.actualizar_cantidad_dialogos();
          const nuevo_dialogo = crear_html_gestion(dialogo_data);
          if (gestiones_agente.hasChildNodes()) {
            const first_child = gestiones_agente.firstElementChild;
            gestiones_agente.insertBefore(nuevo_dialogo, first_child);
          } else {
            gestiones_agente.appendChild(nuevo_dialogo);
          }
        } else {
          if (dialogo.style.display == 'none'){
            dialogo.parentNode.removeChild(dialogo); //OJO ASI PARA ELIMINAR SI ANTES HABIA ESTADO EEN LA PAGINA
            console.log("ELIMINANDO mensaje ya gestionado");
            dialogo = null;
          }
          else {
            // console.log("NO TIENE DISPLAY NONE");
          }
          var dialogo_activo = div_contacto.getAttribute('data-id');

          if (dialogo_data['mensajes_nuevos'] > 0) {
            // console.log(">>>>> si nuevo");
            let span_cant_mensajes = dialogo.getElementsByClassName('badge')[0];
            // console.log("CANTIDAD MSJS : "+ Number(span_cant_mensajes.textContent) );
            let cantidad_mensajes = Number(span_cant_mensajes.textContent);
            if (dialogo_data['mensajes_nuevos'] > cantidad_mensajes) {
              // console.log("++++ aumento ");
              span_cant_mensajes.textContent = dialogo_data['mensajes_nuevos'];
              span_cant_mensajes.style.display = 'block';
              let span_fecha_mensajes = dialogo.children[0].children[1].children[2].children[0];
              span_fecha_mensajes.textContent = dialogo_data['fecha_modificacion'];
              gestiones_agente.removeChild(dialogo);
              const first_child = gestiones_agente.firstElementChild;
              gestiones_agente.insertBefore(dialogo, first_child);
              mensaje = "Nuevo mensaje de: " + dialogo_data['nombre_largo'] ;
              showNotification(mensaje);

            }
            if (dialogo_activo == dialogo_data['id']) {
              console.log("Dialogo Activo: " + dialogo_data['id']);
              cargar_mensajes_dialogo(dialogo_data['id'], 'no_leido');
            }
            else{
              // console.log("noactive: " + dialogo_data['id']);
            }
          }
          else{
            // console.log("_______NO...VIEJO");

          }



        }
      }
      else{
        try{
          const dialogo = document.getElementById(`gestion-${dialogo_data['id']}`);
          gestiones_agente.removeChild(dialogo);
          // console.log("elimino uno que no corresponde: ", dialogo);
        } catch (error) {
          // console.error(error);
          // expected output: ReferenceError: nonExistentFunction is not defined
          // Note - error messages will vary depending on browser
        }
      }

    });

    dialogos_nuevos.forEach((dialogo_data) => {
      const dialogo = document.getElementById(`gestion-${dialogo_data['id']}`);
      const id_campana_whatsapp = dialogo_data['id_campana'];
      // const agente_camapana_id = Number(document.getElementById('id_campana_whatsapp').value)
      try{
        if (dialogo.style.display == 'none'){
          dialogo.parentNode.removeChild(dialogo); //OJO ASI PARA ELIMINAR SI ANTES HABIA ESTADO EEN LA PAGINA
          console.log("ELIMINANDO mensaje ya gestionado");
          dialogo = null;
        }
      }
      catch (error) {
        console.log("era nulo");
      }

      // console.log("DIALOGO: updating.... ", dialogo );
      // console.log("AGENTE CAMOPANA ID: .... ", agente_camapana_id );

      if (dialogo_pertece_a_campana(id_campana_whatsapp)){ //VALIDO CON LA FUNCION SI EL DIALOGO ESXISTE EN LAS CAMPANAS DEL AGENTE
        // console.log(">>>>>>>>> agregar_dialogos >> SI PERTENECE DIALOGO A MI CAMPAÑA ");
        if (dialogo == null) {
          this.aumentar_dialogos_nuevos(1);
          this.actualizar_cantidad_dialogos();
          // console.log("13. CREANDO CON LA DATA: ", dialogo_data);
          const nuevo_dialogo = crear_html_gestion(dialogo_data);
          if (gestiones_cola.hasChildNodes()) {
            const first_child = gestiones_cola.firstElementChild;
            gestiones_cola.insertBefore(nuevo_dialogo, first_child);
          } else {
            gestiones_cola.appendChild(nuevo_dialogo);
          }
        }
        else {
          const div_cuenta_mensajes = dialogo.children[0].children[1].children[0].children[0].children[1];
          const no_mensajes_actual = Number(dialogo.children[0].children[1].children[0].children[0].children[1].innerText)
          const no_mensajes_entrantes = dialogo_data['mensajes_nuevos'];
          // console.log("MENSAJES ACTUALES: ", no_mensajes_actual);
          // console.log("MENSAJES ENTRANTES: ", no_mensajes_entrantes);
          //VERIFICO SI AUMENTO EL NUMERO DE MENSAJES PARA DESTRUIR Y SUBIR EL DIALOGO
          if (no_mensajes_actual < no_mensajes_entrantes) {
            console.log("Reordenando el DIALOGO en la cola de los nuevos: ", dialogo);
            div_cuenta_mensajes.textContent = dialogo_data['mensajes_nuevos'];
            let span_fecha_mensajes = dialogo.children[0].children[1].children[2].children[0];
            span_fecha_mensajes.textContent = dialogo_data['fecha_modificacion'];
            const first_child = gestiones_cola.firstElementChild;
            gestiones_cola.insertBefore(dialogo, first_child);


          }

        }
      }
      else{
        console.log(">> agregar_dialogos > dialogo no corresponde a campaña");
      }
    });
  }

  crear_dialogo() {
    const codigo_pais = document.getElementById('codigo_pais').value;
    const numero = document.getElementById('numero_nuevo').value;
    if (numero == '' || numero == undefined) {
      document.getElementById('error_numero_nuevo').style.display = 'block';
    } else {
      document.getElementById('numero_nuevo').value = '';
      document.getElementById('error_numero_nuevo').style.display = 'none';
      ventana_nuevo_chat.classList.toggle('active');
      const data_dialogo = {
        'id': `${codigo_pais}${numero}@c.us`,
        'estado': 'en_gestion',
        'nombre': `${codigo_pais}${numero}`,
        'numero': `${codigo_pais}${numero}`,
        'imagen': '',
        'mensaje_inicial': 0,
        'nombre_corto': `${codigo_pais}${numero}`,
        'fecha_modificacion': 'xxx xx. xxxx, x:x XX'
      }
      const nuevo_dialogo = crear_html_dialogo(data_dialogo);
      this.cargar_mensajes_dialogo([], true);
      dialogo_guardar_gestion.setAttribute('data-id', id_dialogo);
      dialogo_guardar_gestion.setAttribute('data-calldata', call_data_json);
      this.gestionar_dialogo(nuevo_dialogo, data_dialogo);
    }
  }

}

const interfaz_whatsapp = new InterfazWhatsapp(
  cantidad_gestion = Number(cantidad_dialogos_gestion.innerHTML),
  cantidad_nuevos = Number(cantidad_dialogos_nuevos.innerHTML),
);
/*
*******************************************************************
* Eventos
*******************************************************************
*/
document.addEventListener('DOMContentLoaded', (e) => {
  socket_agente = conectar_chatapi();

  let elementos_gestion = document.querySelectorAll('#main-left-col .whatsapp');
  elementos_gestion.forEach((elemento) => {
    let id_gestion_whatsapp = elemento.getAttribute('data-id');
    elemento.addEventListener('click', () => {

      cargar_mensajes_dialogo(id_gestion_whatsapp);
      select_this_box(id_gestion_whatsapp);
      document.getElementById('input-mensajes').value = '';
      document.querySelector('.respuestas-rapidas div').classList.value = '';
      document.querySelector('.respuestas-rapidas div').classList.textContent = '';
      document.querySelector('.respuestas-rapidas div').classList.value = 'desactive';
    });
  });

  let elementos_telefonia = document.querySelectorAll('#main-left-col .phone');
  elementos_telefonia.forEach((elemento) => {
    let id_gestion_telefonia = elemento.getAttribute('data-id');
    elemento.addEventListener('click', () => {
      select_this_box(id_gestion_telefonia);
    });
  });

});

document.addEventListener('SendWhastAppIframe', enviar_whatsapp_iframe, false);

['change', 'keyup'].forEach((evento) => {
  nuevo_mensaje.addEventListener(evento, render_mensaje);
});

nuevo_mensaje.addEventListener('keyup',()=>{
  if(nuevo_mensaje.value.startsWith('\n')) {
      nuevo_mensaje.value=''
  }
})
btn_enviar_mensaje.addEventListener('click', () => {
  let mensaje = nuevo_mensaje.value;
  // console.log(mensaje);
  if (mensaje.startsWith('\n')){

  }else{
    if (mensaje != '') {
      enviar_mensaje(mensaje);
    }
  }

});

window.addEventListener("paste", processEvent);


function processEvent(e) {



    for (var i = 0; i < e.clipboardData.items.length; i++) {
        // get the clipboard item
        // obtengo el clipboard item
        var clipboardItem = e.clipboardData.items[0];
        var type = clipboardItem.type;

        // verifico si es una imagen
        if (type.indexOf("image") != -1) {
            console.log('se va a pegar la imagen :');
            $('#modal-ctrlv').modal('show'); //SIE SUNA IMAGEN LEVANTO EL MODAL SI NO, NO,
            // obtengo el contenido de la imagen BLOB
            var blob = clipboardItem.getAsFile();
            console.log("blob", blob);
            // creo un la URL del objeto
            var blobUrl = URL.createObjectURL(blob);
            console.log("blobUrl", blobUrl);
            // agrego la captura a mi imagen
            // document.getElementById('id_ctrlv')[0].setAttribute('src', blobUrl)
            var xhr = new XMLHttpRequest();
            var reader = new FileReader();
            reader.readAsDataURL(blob);
            reader.onloadend = function() {
                var base64data = reader.result;
                console.log(base64data);
                imagen_papele.style.display = 'block';
                imagen_papele.setAttribute('src', base64data);

            }


            // document.getElementsByTagName("img")[0].setAttribute("src", blobUrl);


        } else {
            console.log("No soportado " + type);
        }
    }
}
btn_enviar_img = document.getElementById('enviar-circle');

btn_enviar_img.addEventListener('click',()=>{
  let caption_imagen = document.getElementById('div_caption');
  const id_dialogo = div_contacto.getAttribute('data-id');
  let tipo = 'image';

      // for (i = 0; i < files.length; i++) {
      // console.log('file es ::: '+files)
      let named_file = 'Captura_pantalla.png';
      // reader.readAsDataURL(files[i]);
      const datos = {
        'accion': 'enviar_mensaje',
        'id_dialogo': id_dialogo,
        'body': imagen_papele.src,
        'filename': named_file,
        'tipo': tipo,
        'id_agente': id_agente,
        'caption': caption_imagen.innerText
      }
      websocket_enviar_datos(datos);
      var today = new Date();
      var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
      var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
      var dateTime = date+' '+time;
      mensaje = {
        "tipo": tipo,
        "from_me": true,
        "archivo": imagen_papele.src,
        "body": imagen_papele.src,
        "agente": username_agente,
        'caption': caption_imagen.innerText,
        'nombre_archivo': named_file,
        "fecha_recibido": dateTime,
      }
      const mensaje_container = crear_html_mensaje(mensaje);

      ventana_mensajes.appendChild(mensaje_container);
      ventana_mensajes.scrollTop = ventana_mensajes.scrollHeight;
      nuevo_mensaje.value = '';
      // cargar_mensajes_dialogo(id_dialogo)

    // }
    if (div_adjuntar_menu.classList.contains('active')) {
      div_adjuntar_menu.classList.toggle('active');
    }
    caption_imagen.innerText = '';
    imagen_papele.removeAttribute('src');
    imagen_papele.style.display = 'none';

    $('#modal-ctrlv').modal('hide');


})

btn_enviar_audio.addEventListener('click', () => {
  enviar_audio();
});

btn_adjuntar.addEventListener('click', () => {
  div_adjuntar_menu.classList.toggle('active');
});

btn_adjuntar_imagen.addEventListener('click', () => {
  input_imagen.click();
});

btn_adjuntar_archivo.addEventListener('click', () => {
  input_archivo.click();
});

input_imagen.addEventListener('change', () => {
  if (input_imagen.files.length > 0) {
    enviar_archivo('image');
  }
});

input_archivo.addEventListener('change', () => {
  if (input_archivo.files.length > 0) {
    enviar_archivo('document');
  }
});

btn_whastapp_validar_mensaje.addEventListener('click', () => {
  let numero_whatsapp = document.getElementById('whatsapp-numero-nuevo').value;
  if (numero_whatsapp == '') {
    document.getElementById('error-nuevo-whatsapp').style.display = 'block';
  }else{
    document.getElementById('error-nuevo-whatsapp').style.display = 'none';
    let numero_validar = "57"+document.getElementById('whatsapp-numero-nuevo').value;
    console.log(">>> btn_whastapp_validar_mensaje >> CONSULTANDO TELEFONO!", numero_validar);
    let url_wp = Urls.tc_validar_whatsapp_ajax();
    var tiene_whatsapp;
    $("#overlay").fadeIn(300);
    $.get(url_wp, {"numero_validar": numero_validar }, function(data,status){
      if (status == "success"){
        $("#overlay").fadeOut(300);
        tiene_whatsapp = data['respuesta'];
        console.log("respuesta del backend", tiene_whatsapp);
        if (tiene_whatsapp == true){
          console.log("si tiene whatsapp");
          let numero_telefono = "57"+document.getElementById('whatsapp-numero-nuevo').value +"@c.us";
          let url = Urls.tc_validar_telefono_ajax();
          $.get(url, {"numero_telefono": numero_telefono }, function(data,status){
            if (status=="success"){
              var respuesta = data['respuesta'];
              console.log("resultados", respuesta )

              if (respuesta['estado'] === 'en_gestion' ){
                console.log("YA EXISTE EL CONTACTO ASESOR!!!")
                var nombre_contacto = respuesta['contacto_full_name'];
                var asesor = respuesta['agente_full_name'];
                swal.fire({
                  icon: 'error',
                  title: 'Contacto ya existe: ' + nombre_contacto,
                  text: 'El Agente asesor: ' + asesor + ' lo esta gestionando'
                })

              }else if (respuesta['estado'] === 'bot' ){
                console.log("YA EXISTE CONTACTO EN SALA ESPERA !!")
                var nombre_contacto = respuesta['contacto_full_name'];
                var asesor = respuesta['agente_full_name'];
                swal.fire({
                  icon: 'error',
                  title: 'Contacto' + nombre_contacto + ' esta en robot en un momento entrara a la cola: ' ,
                  text: 'El Agente asesor: ' + asesor + ' lo esta gestionando'
                })

              }else if (respuesta['estado'] === 'encuesta' ){
                console.log("CONTACTO EN ENCUESTA !!!")
                var nombre_contacto = respuesta['contacto_full_name'];
                var asesor = respuesta['agente_full_name'];
                swal.fire({
                  icon: 'error',
                  title: 'Contacto: ' + nombre_contacto + ' en encuesta en unos minutos estará disponible',
                  text: 'El Agente asesor: ' + asesor + ' lo esta gestionando'
                })

              }else if (respuesta['estado'] === 'nuevo' ){
                console.log("CONTACTO EN COLA !!!")
                var nombre_contacto = respuesta['contacto_full_name'];
                var asesor = respuesta['agente_full_name'];
                swal.fire({
                  icon: 'error',
                  title: 'Contacto: ' + nombre_contacto + ' en espera de ser atendido',
                  text: 'El Usuario : ' + asesor + ' está esperando un agente.'
                })
              }else{
                console.log("no existe todo bien.....Activo modal campaña");
                $('#modal-whatsapp-campana-manual').modal('show');
                document.getElementById('id_chatapi').value =numero_telefono;
                document.getElementById('no_whatsapp').value =numero_validar;
                // MUESTRO EL MODAL Y HASTA AHI

              }
            }else{
              console.log("Error en peticion al servidor")
              swal.fire({
                icon: 'error',
                title: 'Error en el servidor consulte al administrador.',
                text: 'Da2Soft'
              })
            }
            });
        }
        else{
          swal.fire({
            icon: 'error',
            title: 'El numero ingresado no tiene cuenta de whatsapp activa.',
            text: 'Numero: ' + numero_validar
          })

        }

      }
      else{
        swal.fire({
          icon: 'error',
          title: 'Error en el servidor consulte al administrador.',
          text: 'Da2Soft'
        })
      }
    })
    // enviar_nuevo_whatsapp();
  }


});


function Swal_fire_transferir() {
  Swal.fire({
    title: 'Transferir a Campaña , Agente',
    input: 'select',
    inputOptions: {
      '1': 'Campaña',
      '2': 'Agente',
    },
    inputPlaceholder: 'required',
    showCancelButton: true,
    inputValidator: function (value) {
      return new Promise(function (resolve) {
        console.log('El value del select:'+ value);
        if (value !== '') {
          resolve();
        } else {
          resolve('Seleccione una opcion valida');
        }
      });
    }
  }).then((result) => {
    if (result.value == 1){
      $("#modal-transfer-whatsapp-campana").modal('show');
      $('#modal-transfer-whatsapp-gestion').modal('hide');
    }
    else if (result.value == 2){
      $("#modal-transfer-whatsapp-agente").modal('show');
      $('#modal-transfer-whatsapp-campana').modal('hide');
    }
  });
}

btn_transferir_gestion_agente.addEventListener('click', () => {
  const id_agente_transfer = select_transferir_gestion_agente.value;
  if (id_agente_transfer != '') {
    transferir_gestion(id_agente_transfer);
    $("#modal-transfer-whatsapp-agente").modal('hide')
  }
});

btn_transferir_gestion_campana.addEventListener('click', () => {
  const id_campana_transfer = select_transferir_gestion_campana.value;
  if (id_campana_transfer != '') {
    transferir_campana(id_campana_transfer);
    $('#modal-transfer-whatsapp-campana').modal('hide');
  }
});

btn_nuevo_whatsapp.addEventListener('click', () => {
  div_nuevo_whatsapp.classList.toggle('active');
});


/*
*******************************************************************
* drag elements
*******************************************************************
*/

function allowDrop(e) {
  $('#modal-enviar-archivo').modal('show');
  e.preventDefault();
}

function drag(ev) {
  ev.dataTransfer.setData("id_element", ev.target.id);
}

function drop(ev) {
  ev.preventDefault();
  let producto = document.getElementById(ev.dataTransfer.getData("id_element"));
  let img_producto = producto.querySelector('img');
  convertir_enviar_imagen_producto(img_producto.src, producto, enviar_producto);
}

/*
*******************************************************************
* funciones
*******************************************************************
*/

function enviar_whatsapp_iframe(e) {
  let chat_id = e.detail['chat_id'];
  cargar_mensajes_dialogo(chat_id);
}

function conectar_chatapi() {
  const web_socket = new WebSocket(url_chatapi_server);

  web_socket.onopen = () => {
    console.log('Conectado a ChatApiWebSocket !');
    swal.fire({
      icon: 'success',
      title: 'Conexion Whatsapp! Exitosa',
      text: 'Se ha reconectado el servicio ',
      showConfirmButton:false,
      timer:2000,
    })
  }

  web_socket.onmessage = (e) => { //CUANDO LLEGA EL TORNADO<<A<<<<
    data = JSON.parse(e.data);
    // console.log(">>> web_socket.onmessage : ", data);
    const accion = data['accion'];
    if (accion == 'agregar_dialogos') {
      interfaz_whatsapp.agregar_dialogos(data);
    } else if (accion == 'cambiar_estado') {
      const dialogo = document.getElementById(`gestion-${data['id_dialogo']}`);
      let estado_dialogo = dialogo.getAttribute('data-estado');
      if (estado_dialogo == 'nuevo') {
        interfaz_whatsapp.remover_dialogo_nuevos(dialogo);
      }
    }
  }

  web_socket.onerror = (e) => {
    console.log('error al conectar websocket');
    // swal.fire({
    //   icon: 'error',
    //   title: 'Error de Conexion Whatsapp!',
    //   showConfirmButton:false,
    //   timer:2000,
    //   text: 'Se encontro un error al intentar conectar con el servicio, por favor comuniquese con el administrador del sistema'
    // })
  }

  web_socket.onclose = (e) => {
    console.log('conexion cerrada reconectar por error');
    // swal.fire({
    //   icon: 'error',
    //   title: 'Conexion Whatsapp! Interrumpida',
    //   text: 'Se ha desconectado el servicio, los mensajes enviados no llegaran a su destino. Por favor vuelva a ingresar su usuario, si el error persiste por favor comuniquese con el administrador del sistema',
    //   showConfirmButton:false,
    //   timer:2000,
    // })
    reconectar_chatapi();
  }

  return web_socket;
}

function reconectar_chatapi() {
  setTimeout(function () {
    socket_agente = conectar_chatapi();
  }, 10000);
}

function websocket_enviar_datos(datos) {
  const datos_enviar = JSON.stringify(datos);
  socket_agente.send(datos_enviar);
}

function render_mensaje(e) {

  const url = "/tc/respuesta/ajax/consulta/";  //esta es la url donde voy  a consultar

  if (e.keyCode == 13 && !e.shiftKey){

    var p_comparar = document.getElementById('div_comparar');

    if ($(".comparar").toArray().length == 1) {
      let texto_enviar = p_comparar.firstElementChild.getAttribute('texto');
      nuevo_mensaje.value = texto_enviar;
    }

    btn_enviar_mensaje.click();

  }else{
    let mensaje = e.target.value;
    mensaje = mensaje.trim();
    if (div_adjuntar_menu.classList.contains('active')) {
      div_adjuntar_menu.classList.toggle('active');
    }

    if (mensaje == '' || mensaje == null) {
      btn_enviar_audio.style.display = 'block';
      btn_enviar_mensaje.style.display = 'none';
      document.querySelector('.respuestas-rapidas div').classList.value = '';
      document.querySelector('.respuestas-rapidas div').textContent = '';

    } else {

      if (mensaje.startsWith('/')) {

        var metadata=  document.getElementById('id_metadata').value

        $.get(url, {"atajo": mensaje , "metadata" : metadata }, function(data,status){
          if (status == "success"){
            var respuesta = data['respuesta'];
            if (respuesta == ''){
              document.querySelector('.respuestas-rapidas div').classList.value = '';
              document.querySelector('.respuestas-rapidas div').textContent = '';
            }else{
              document.querySelector('.respuestas-rapidas div').textContent = '';
              for (i=0; i<respuesta.length; i++){
                var datos = respuesta[i];
                const p = document.createElement('p');
                p.innerHTML = `
                  <span texto="${datos['texto']}">
                    <strong texto="${datos['texto']}">${datos['atajo']}</strong>
                    ${datos['texto']}
                  </span>`;
                p.setAttribute('texto', datos['texto']);
                p.setAttribute('class', 'comparar');
                console.log("Estoy agregando un elemento al div");
                // conts btn = document.createElement('btn');
                document.querySelector('.respuestas-rapidas div').appendChild(p);


                p.addEventListener('dblclick', function(e) {
                    let texto_descripcion = e.target.getAttribute('texto');
                    nuevo_mensaje.value = texto_descripcion;
                    nuevo_mensaje.focus();
                    document.querySelector('.respuestas-rapidas div').classList.value = '';
                });


              };

              document.querySelector('.respuestas-rapidas div').classList.value = 'active';
            }
          }else{
            console.log("Error al traer las respuestas rapidas");
          }

        });

      }else{
        document.querySelector('.respuestas-rapidas div').classList.value = '';
        document.querySelector('.respuestas-rapidas div').textContent = '';
      }
      btn_enviar_audio.style.display = 'none';
      btn_enviar_mensaje.style.display = 'block';
    }
  }
}

// function enviar_nuevo_whatsapp(){
//   let codigo_pais = document.getElementById('whatsapp-codigo-pais').value;
//   let numero_whatsapp = document.getElementById('whatsapp-numero-nuevo').value;
//   if (numero_whatsapp == '') {
//     document.getElementById('error-nuevo-whatsapp').style.display = 'block';
//   }else{
//     document.getElementById('error-nuevo-whatsapp').style.display = 'none';
//     let chat_id = codigo_pais.concat(numero_whatsapp, '@c.us');
//     div_nuevo_whatsapp.classList.toggle('active');
//     document.getElementById('whatsapp-numero-nuevo').value = '';
//     cargar_mensajes_dialogo(id_dialogo=chat_id);
//   }
// }

function enviar_mensaje(mensaje_text) {
  const id_dialogo = div_contacto.getAttribute('data-id');
  const tipo = 'chat';
  const body_mensaje = mensaje_text;
  const datos = {
    'accion': 'enviar_mensaje',
    'id_dialogo': id_dialogo,
    'body': body_mensaje,
    'tipo': tipo,
    'id_agente': id_agente,
    "fecha_recibido": dateTime
  }

  websocket_enviar_datos(datos);

  var today = new Date();
  var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  var dateTime = date+' '+time;

  mensaje = {
    "tipo": tipo,
    "from_me": true,
    "archivo": "",
    "body": body_mensaje,
    "agente": username_agente,
    "fecha_recibido": dateTime
  }
  const mensaje_container = crear_html_mensaje(mensaje);
  ventana_mensajes.appendChild(mensaje_container);
  ventana_mensajes.scrollTop = ventana_mensajes.scrollHeight;
  nuevo_mensaje.value = '';
  document.querySelector('.respuestas-rapidas div').textContent = '';
  document.querySelector('.respuestas-rapidas div').classList.value = '';
  btn_enviar_audio.style.display = 'block';
  btn_enviar_mensaje.style.display = 'none';

}

function enviar_archivo(tipo) {
  const id_dialogo = div_contacto.getAttribute('data-id');
  let reader = new FileReader();
  let files = '';
  let procesar_envio = false;
  let caption = '';

  switch (tipo) {
    case 'image':
      files = input_imagen.files;
      procesar_envio = true;
      caption = '';
      break;
    case 'document':
      files = input_archivo.files;
      procesar_envio = true;
      break;
    default:
      console.log('No se reconoce el tipo de archivo a enviar!');
  }
  if (procesar_envio) {
    for (i = 0; i < files.length; i++) {
      let named_file = files[i]['name'];
      reader.readAsDataURL(files[i]);
      reader.onload = function () {
        const datos = {
          'accion': 'enviar_mensaje',
          'id_dialogo': id_dialogo,
          'body': reader.result,
          'filename': named_file,
          'tipo': tipo,
          'id_agente': id_agente,
          'caption': caption
        }
        websocket_enviar_datos(datos);
        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date+' '+time;
        mensaje = {
          "tipo": tipo,
          "from_me": true,
          "archivo": reader.result,
          "body": reader.result,
          "agente": username_agente,
          'caption': caption,
          'nombre_archivo': named_file,
          "fecha_recibido": dateTime
        }
        const mensaje_container = crear_html_mensaje(mensaje);
        ventana_mensajes.appendChild(mensaje_container);
        ventana_mensajes.scrollTop = ventana_mensajes.scrollHeight;
        nuevo_mensaje.value = '';
      }
      reader.onerror = function (error) {
        console.log('Error ', error);
      }
    }
    if (div_adjuntar_menu.classList.contains('active')) {
      div_adjuntar_menu.classList.toggle('active');
    }
  }
}

function enviar_producto(producto, img_base64) {
  const id_dialogo = div_contacto.getAttribute('data-id');
  let tipo = 'image';
  let nombre_producto = producto.getAttribute('nombre-producto');
  let valor_producto = producto.getAttribute('valor-producto');

  let caption = `producto: ${nombre_producto}\nvalor_producto: ${valor_producto}`;

  named_file = `${nombre_producto}.${producto.querySelector('img').src.split('.').pop()}`;

  const datos = {
    'accion': 'enviar_mensaje',
    'id_dialogo': id_dialogo,
    'body': img_base64,
    'filename': named_file,
    'tipo': tipo,
    'id_agente': id_agente,
    'caption': caption
  }

  websocket_enviar_datos(datos);
  var today = new Date();
  var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
  var dateTime = date+' '+time;

  mensaje = {
    "tipo": tipo,
    "from_me": true,
    "archivo": img_base64,
    "body": img_base64,
    "agente": username_agente,
    'caption': caption,
    'nombre_archivo': named_file,
    "fecha_recibido": dateTime,
  }
  const mensaje_container = crear_html_mensaje(mensaje);
  ventana_mensajes.appendChild(mensaje_container);
  ventana_mensajes.scrollTop = ventana_mensajes.scrollHeight;
  nuevo_mensaje.value = '';

}

function enviar_audio(e) {
  console.log('enviando audio');
  const duracion = document.getElementById('duracion');
  var options = {
    encoderPath: "/static/truecontact/truecontact/js/completeRecord/encoderWorker.min.js"
  };

  var recorder = new Recorder(options);
  recorder.start();

  const id_dialogo = div_contacto.getAttribute('data-id');
  let procesar_envio = false;
  const tipo = 'ptt';
  div_audio.style.display = 'block';
  document.querySelector('#enviar-nuevo-mensaje .input-group-text').style.display = 'none';
  btn_enviar_audio.style.display = 'none';




  // pause.addEventListener( "click", function(){
  //     recorder.state="inactive";
  //     recorder.onstop();
  // });

  btn_cancelar_audio.addEventListener("click", () => {
    recorder.stop();
    div_audio.style.display = 'none';
    document.querySelector('#enviar-nuevo-mensaje .input-group-text').style.display = 'block';
    btn_enviar_audio.style.display = 'block';
  });

  btn_confirmar_audio.addEventListener("click", () => {
    procesar_envio = true;
    recorder.stop();
    div_audio.style.display = 'none';
    document.querySelector('#enviar-nuevo-mensaje .input-group-text').style.display = 'block';
    btn_enviar_audio.style.display = 'block';
  });

  recorder.onstart = function (e) {
    comenzarAContar();
  };

  recorder.onstop = function (e) {
    detenerConteo();
  };

  recorder.ondataavailable = function (typedArray) {
    if (procesar_envio) {
      let dataBlob = new Blob([typedArray], { type: 'audio/ogg' });
      const reader = new FileReader();
      reader.readAsDataURL(dataBlob);
      reader.onload = () => {
        d = new Date();
        dformat = ' F ' + [
          d.getMonth() + 1,
          d.getDate(),
          d.getFullYear()
        ].join('-') + ' H ' + [
          d.getHours(),
          d.getMinutes(),
          d.getSeconds()
        ].join('-');

        let named_file = dformat + ".ogg";

        const datos = {
          'accion': 'enviar_mensaje',
          'id_dialogo': id_dialogo,
          'body': reader.result,
          'filename': named_file,
          'tipo': tipo,
          'id_agente': id_agente,
          'caption': ''
        }

        websocket_enviar_datos(datos);

        var today = new Date();
        var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var dateTime = date+' '+time;

        mensaje = {
          "tipo": tipo,
          "from_me": true,
          "archivo": reader.result,
          "body": reader.result,
          "agente": username_agente,
          'caption': '',
          'nombre_archivo': named_file,
          "fecha_recibido": dateTime,
        }
        const mensaje_container = crear_html_mensaje(mensaje);
        ventana_mensajes.appendChild(mensaje_container);
        ventana_mensajes.scrollTop = ventana_mensajes.scrollHeight;
        nuevo_mensaje.value = '';
      }
    }
  }

  const segundosATiempo = numeroDeSegundos => {
    let horas = Math.floor(numeroDeSegundos / 60 / 60);
    numeroDeSegundos -= horas * 60 * 60;
    let minutos = Math.floor(numeroDeSegundos / 60);
    numeroDeSegundos -= minutos * 60;
    numeroDeSegundos = parseInt(numeroDeSegundos);
    if (horas < 10) horas = "0" + horas;
    if (minutos < 10) minutos = "0" + minutos;
    if (numeroDeSegundos < 10) numeroDeSegundos = "0" + numeroDeSegundos;
    return `${horas}:${minutos}:${numeroDeSegundos}`;
  };

  const refrescar = () => { duracion.textContent = segundosATiempo((Date.now() - tiempoInicio) / 1000); }

  // Ayudante para la duración; no ayuda en nada pero muestra algo informativo
  const comenzarAContar = () => {
    tiempoInicio = Date.now();
    idIntervalo = setInterval(refrescar, 500);
  };

  const detenerConteo = () => {
    clearInterval(idIntervalo);
    tiempoInicio = null;
    duracion.textContent = "00:00:00";
  }
}



function transferir_gestion(id_agente_transfer) {
  const id_dialogo = div_contacto.getAttribute('data-id');
  const dialogo = document.getElementById(`gestion-${id_dialogo}`);
  interfaz_whatsapp.remover_dialogo_gestion(dialogo);
  // aqui debo ver que hago con la vista principal
  const datos = {
    'accion': 'transferir_dialogo',
    'id_dialogo': id_dialogo,
    'id_agente': id_agente_transfer
  }
  websocket_enviar_datos(datos);
  main_gestion.style.display = 'none';
  main_crm.style.display = 'block';

}

function transferir_campana(id_campana_transfer) {
  const id_dialogo = div_contacto.getAttribute('data-id');
  const dialogo = document.getElementById(`gestion-${id_dialogo}`);
  interfaz_whatsapp.remover_dialogo_gestion(dialogo);
  // aqui debo ver que hago con la vista principal
  const datos = {
    'accion': 'transferir_dialogo',
    'id_dialogo': id_dialogo,
    'id_agente': '',
    'id_campana': id_campana_transfer
  }
  websocket_enviar_datos(datos);
  main_gestion.style.display = 'none';
  main_crm.style.display = 'block';



}

function cargar_mensajes_dialogo(id_dialogo, estado = 'todos') {
  console.log(">>> cargar_mensajes_dialogo > 1.CARGAR_MENSAJE_DIALOGO");
  let string_url = window.location.origin + Urls.tc_dialogo_mensajes(id_dialogo);
  let url = new URL(string_url),
    params = { 'estado': estado };

  Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
  fetch(url, {
    method: 'get',
  })
    .then(function (res) {
      return res.json();
    })
    .then(function (respuesta) {
      let puede_gestionar = respuesta['puede_gestionar'];
      if (puede_gestionar) {
        let mensajes_list = respuesta['mensajes'];
        let dialogo = document.getElementById(`gestion-${id_dialogo}`);
        let dialogo_info = respuesta['dialogo'];
        // console.log(">>> cargar_mensajes_dialogo > 2.Rta del DIALOGO MENSAJESLIST", dialogo_info);

        if (dialogo === null) {    //SI EL DIALOGO NO EXISTE ES UNA GESTION DE CEROS
          // console.log(">>> cargar_mensajes_dialogo > 2.A GESTION_DE_CEROS")
          dialogo = crear_html_gestion(dialogo_info);
          interfaz_whatsapp.agregar_dialogo_gestion(dialogo);
          var origen = dialogo.getAttribute('data-origen-gestion')
          if( origen != ''){
            var origen_gestion = JSON.parse(origen);
          }
          const call_data_new = {
            'medio': dialogo_info['medio'],
            'id_contacto': dialogo_info['id_contacto'],
            'telefono': dialogo_info['numero'].slice(-10),
            'call_type': '4',
            'chat_id': dialogo_info['id'],
            'mensaje_inicial': dialogo_info['mensaje_inicial'],
            'id_campana': dialogo_info['id_campana'],
            'modelo_gestionar': origen_gestion
          };
          agregar_gestion_whatsapp(call_data_new);  //AGREGO LA GESTION DE WHATSAPP
        }                 // END --- SI EL DIALOGO NO EXISTE ES UNA GESTION DE CEROS
        else {
          if (dialogo_info['pk_gestion'] == '' ) {   //SI EL PK NO EXISTE ES UNA GESTION entrante cuando Contacto pertenece al agente
            console.log(">>> cargar_mensajes_dialogo > 2.B DIALOGO_INFO:", dialogo_info);
            var origen = dialogo.getAttribute('data-origen-gestion');
            if( origen != ''){
              var origen_gestion = JSON.parse(origen);
            }

            const call_data_new = {
              'medio': dialogo_info['medio'],
              'id_contacto': dialogo_info['id_contacto'],
              'telefono': dialogo_info['numero'].slice(-10),
              'call_type': '4',
              'chat_id': dialogo_info['id'],
              'mensaje_inicial': dialogo_info['mensaje_inicial'],
              'id_campana': dialogo_info['id_campana'],
              'modelo_gestionar': origen_gestion

            };
            agregar_gestion_whatsapp(call_data_new);  //AGREGO LA GESTION Contacto DE WHATSAPP
            console.log("Empieza delay");
            sleep(300).then(() => {
              console.log(">>> cargar_mensajes_dialogo > 3. VOLVIO DE GUARDAR GESTION WHATSAPP");
              let id_gestion = dialogo.getAttribute('data-gestion');
              console.log(">>> cargar_mensajes_dialogo > 3.1 ID GESTION ES: "+""+id_gestion )
            });

          }
          else{

            pk_gestion = dialogo_info['pk_gestion'];
            dialogo.setAttribute('data-gestion',pk_gestion)
            cargar_gestion_contacto(pk_gestion); // renderiso su gestion
            console.log(">>> cargar_mensajes_dialogo > 3.2 ID GESTION CARGADO EXITOSAMENTE: " +  pk_gestion )
           }
        }
        var origen = dialogo.getAttribute('data-origen-gestion')
        if (origen == null){
          if (var_global != ''){
            var origen_gestion = JSON.parse(var_global);
            var call_data_new = {
              'medio': dialogo_info['medio'],
              'id_contacto': dialogo_info['id_contacto'],
              'telefono': dialogo_info['numero'].slice(-10),
              'call_type': '4',
              'chat_id': dialogo_info['id'],
              'mensaje_inicial': dialogo_info['mensaje_inicial'],
              'id_campana': id_campana_whatsapp,
              'modelo_gestionar': origen_gestion
            };
            // console.log('>>> cargar_mensajes_dialogo > Este es el calldata si no hay origen en la cajita: ', call_data_new);
          }
        }
        else{

          var origen_gestion = JSON.parse(origen);
          var call_data_new = {
            'medio': dialogo_info['medio'],
            'id_contacto': dialogo_info['id_contacto'],
            'telefono': dialogo_info['numero'].slice(-10),
            'call_type': '4',
            'chat_id': dialogo_info['id'],
            'mensaje_inicial': dialogo_info['mensaje_inicial'],
            'id_campana': id_campana_whatsapp,
            'modelo_gestionar': origen_gestion
          };
          // console.log('>>> cargar_mensajes_dialogo > Este es el calldata si tenia origen la cajita: ', call_data_new);
        }

        if (estado == 'todos') {
          interfaz_whatsapp.cargar_mensajes_dialogo(mensajes_list, true);
        } else {
          interfaz_whatsapp.cargar_mensajes_dialogo(mensajes_list);
        }

        let estado_dialogo = dialogo.getAttribute('data-estado');
        if (estado_dialogo === 'nuevo') {   // si es nuevo o es de CONTACTO_ASOCIADO_AGENTE = True
          // console.log(">>> cargar_mensajes_dialogo > 2.C DIALOGO NUEVO");
          // agregar_gestion_whatsapp(call_data);  //AGREGO LA GESTION DE WHATSAPP
          // //delay
          // // console.log("Empieza delay");
          // sleep(100).then(() => {
          //   console.log("ya arranco");
          //   let id_gestion = dialogo.getAttribute('data-gestion');
          //   cargar_gestion_contacto(id_gestion); // renderiso su gestion
          // })

        }

        else{
          // console.log(">>> cargar_mensajes_dialogo > 2.D DIALOGO EN GESTION");
          let id_gestion = dialogo.getAttribute('data-gestion');
          cargar_gestion_contacto(id_gestion); // renderiso su gestion
          div_contacto.setAttribute('data-id', id_dialogo);
          div_contacto.setAttribute('data-calldata', JSON.stringify(call_data_new));
          div_contacto.setAttribute('contacto-pk', dialogo_info['id_contacto']); //en el baner contacto vacio si no existe

        }

        interfaz_whatsapp.gestionar_dialogo(dialogo, dialogo_info);
        identificar_medio(call_data_new); // consola_agente.js

        var global_origen = window.parent.document.getElementById('modelo_origen_gestion');
        var dic_modelo = {'origen': 'None', 'pk': ''}
        var dic_nuevo = JSON.stringify(dic_modelo);
        global_origen.value = dic_nuevo;
        console.log('borreeee el valor de origen de gestion', global_origen.value)

    }else{
      console.log('No se puede gestionar...')
        let dialogo = document.getElementById(`gestion-${id_dialogo}`);
        dialogo.style.display = 'none';
        swal.fire({
          icon: 'info',
          title: 'Contacto ya se encuentra en gestion!',
          text: 'por otro asesor!'
        })
      }

    })
    .catch(function (error) {
      console.log(error);
    });
};

function agregar_gestion_whatsapp(call_data) {  //se ejecuta cuando contesto el Mensaje
  let url = Urls.tc_contacto_ajax_gestionar();
  let call_data_json = JSON.stringify(call_data);
  // let date = new Date();
  console.log(">>>> agregar_gestion_whatsapp >> 2.5 AGREGAR GESTION DE TIPO WHATSAPP");

  $.get(url, { call_data_json }, function (data, status) {  // funcion asincrona Lo mando a aejecutar pero me toca tener cuidado que esto va y espera un tiempo mientras el resto se sigue ejecutando
    if (status == "success") {

      let datos = data['respuesta'];
      let id_gestion = datos['gestion'];
      call_data['id_gestion'] = id_gestion;
      div_contacto.setAttribute('data-id', call_data['chat_id']);
      div_contacto.setAttribute('data-calldata', JSON.stringify(call_data));
      
      // this.cargar_mensajes_dialogo([], true);
      ventana_mensajes.textContent = '';
      cargar_gestion_contacto(id_gestion); // renderiso su gestion

      document.getElementById("id_gestioncontacto").value = datos['gestion']; //para tomar bien el pedido del nuevo

    } else {
      alert("Error del servidor consulte al administrador");
    }

  });
}

function crear_html_mensaje(mensaje) {

  // console.log(mensaje);

  /*
  let mensaje = {
    'autor': 'William Alexander Moreno',
    'tipo': 'chat',
    'body': 'Este es un mensaje de whatsapp',
    'from_me': true,
    'fecha_recibido': 'Nov. 3, 2020, 9:31 AM'
  }
  */



  const row_container = document.createElement('div');
  const div_container = document.createElement('div');
  const div_mensaje_clase = document.createElement('div');
  const div_mensaje_tipo = document.createElement('div');

  const div_autor = document.createElement('div');
  div_autor.setAttribute('style', 'overflow: hidden');

  const autor = document.createElement('span');
  autor.className = 'sender-message-name';
  div_autor.appendChild(autor);

  const div_fecha_mensaje = document.createElement('div');
  div_fecha_mensaje.className = 'time-message';

  const fecha_mensaje = document.createElement('span');
  const estado_mensaje = document.createElement('i');
  fecha_mensaje.textContent = mensaje.fecha_recibido;
  estado_mensaje.className = "flaticon-double-check ml-2";
  // console.log("colocando el nesnaje......");
  div_fecha_mensaje.appendChild(fecha_mensaje);
  div_fecha_mensaje.appendChild(estado_mensaje);

  div_mensaje_tipo.appendChild(div_autor);

  div_mensaje_clase.appendChild(div_mensaje_tipo);
  div_container.appendChild(div_mensaje_clase);
  row_container.appendChild(div_container);

  if (mensaje.tipo == 'chat') {
    if (!mensaje['tiene_mensaje_asociado']) {
      div_mensaje_tipo.className = 'text-message';
      const span_text = document.createElement('span');
      span_text.innerText = mensaje.body;
      div_mensaje_tipo.appendChild(span_text);

    } else {
      let mensaje_asociado = mensaje['mensaje_asociado'];
      div_mensaje_tipo.className = 'forward-message';
      const div_media = document.createElement('div');
      div_media.classList.value = "media d-block d-sm-flex text-sm-left text-center";
      div_mensaje_tipo.appendChild(div_media);

      const div_media_body = document.createElement('div');
      div_media_body.className = 'media-body';
      div_media.appendChild(div_media_body);

      const p_body = document.createElement('p');
      p_body.className = 'media-text';

      if (mensaje_asociado['tipo'] == 'image') {

        const icon_text = document.createElement('i');
        icon_text.classList.add('flaticon-picture', 'mr-1');

        const span_text = document.createElement('span');
        span_text.textContent = mensaje_asociado['caption'];

        p_body.appendChild(icon_text);
        p_body.appendChild(span_text);

        const img = document.createElement('img');
        img.classList.add('ml-sm-3', 'rounded');
        img.setAttribute('src', mensaje_asociado['body']);

        div_media_body.appendChild(p_body);

        div_media.appendChild(img);

      } else if (mensaje_asociado['tipo'] == 'chat') {
        const icon_text = document.createElement('i');
        icon_text.classList.add('flaticon-chat-2', 'mr-1');

        const span_text = document.createElement('span');
        span_text.textContent = mensaje_asociado['body'];

        p_body.appendChild(icon_text);
        p_body.appendChild(span_text);
        div_media_body.appendChild(p_body);


      } else if (mensaje_asociado['tipo'] == 'document') {
        const icon_text = document.createElement('i');
        icon_text.classList.add('far', 'fa-file-alt', 'mr-1');

        const span_text = document.createElement('span');
        span_text.textContent = mensaje_asociado['caption'];

        p_body.appendChild(icon_text);
        p_body.appendChild(span_text);
        div_media_body.appendChild(p_body);
      } else if (mensaje_asociado['tipo'] == 'ppt') {
        const icon_text = document.createElement('i');
        icon_text.classList.add('flaticon-microphone', 'mr-1');

        const span_text = document.createElement('span');
        span_text.textContent = 'audio';

        p_body.appendChild(icon_text);
        p_body.appendChild(span_text);
        div_media_body.appendChild(p_body);
      }

      const div_caption = document.createElement('div');
      const span_caption = document.createElement('span');
      span_caption.classList.add('message-caption');
      span_caption.textContent = mensaje['body'];

      div_caption.appendChild(span_caption);
      div_mensaje_clase.appendChild(div_caption);

    }


  } else if (mensaje.tipo == 'ptt') {
    div_mensaje_tipo.className = 'document-message';

    const div_audio = document.createElement('div');
    div_audio.className = 'text-center';

    const audio = document.createElement('audio');
    audio.controls = 'controls';
    audio.src = mensaje.archivo;
    div_audio.appendChild(audio);
    div_mensaje_tipo.appendChild(div_audio);

  } else if (mensaje.tipo == 'audio') {
    div_mensaje_tipo.className = 'document-message';

    const div_audio = document.createElement('div');
    div_audio.className = 'text-center';

    const audio = document.createElement('audio');
    audio.controls = 'controls';
    audio.src = mensaje.archivo;
    div_audio.appendChild(audio);
    div_mensaje_tipo.appendChild(div_audio);

  } else if (mensaje['tipo'] == 'document') {
    div_mensaje_tipo.className = 'document-message';

    const div_document_body = document.createElement('div');
    div_document_body.className = 'document-body';

    const link = document.createElement('a');
    link.setAttribute('href', mensaje.archivo);
    link.setAttribute('target', '_blank');

    const span_principal = document.createElement('span');
    span_principal.classList.add('d-flex', 'justify-content-center');
    let nombre_archivo_tmp = mensaje['nombre_archivo'];
    let nombre_archivo = '';

    if (nombre_archivo_tmp.length > 24) {
      nombre_archivo = nombre_archivo_tmp.substring(0, 24) + '...';
    } else {
      nombre_archivo = nombre_archivo_tmp;
    }

    span_principal.innerHTML = `
          <span class="media d-sm-flex d-block">
            <span class="ml-2 mr-2">
              <i class="fas fa-file-alt"></i>
            </span>
            <span class="media-body">
              <span>
                ${nombre_archivo}
              </span>
              <span class="mr-2 ml-2">
                <i class="fas fa-download float-right"></i>
              </span>
            </span>
          </span>
        `;

    link.appendChild(span_principal);
    div_document_body.appendChild(link);
    div_mensaje_tipo.appendChild(div_document_body);

  } else if (mensaje['tipo'] == 'image') {
    div_mensaje_tipo.className = 'image-message';

    const div_image_body = document.createElement('div');
    div_image_body.className = 'image-message';

    const link = document.createElement('a');
    link.setAttribute('href', mensaje.archivo);
    link.setAttribute('target', '_blank');

    const imagen = document.createElement('img');
    imagen.setAttribute('src', mensaje['archivo']);
    imagen.setAttribute('style', 'width: 100%');

    link.appendChild(imagen);
    div_image_body.appendChild(link);
    div_mensaje_tipo.appendChild(div_image_body);

  } else if (mensaje['tipo'] == 'video' ) {
    div_mensaje_tipo.className = 'image-message';

    const div_image_body = document.createElement('div');
    div_image_body.className = 'image-message';

    const link = document.createElement('a');
    link.setAttribute('href', mensaje.archivo);
    link.setAttribute('target', '_blank');


    const video = `
      <video width="320" height="240" controls>
        <source src="${mensaje.archivo}">
      </video>
    `;

    link.innerHTML = video;
    div_image_body.appendChild(link);
    div_mensaje_tipo.appendChild(div_image_body);
  }


  if (mensaje.from_me == true) {
    div_mensaje_clase.className = 'message-send';
    div_container.className = 'col-sm-11 justify-content-end d-flex';
    row_container.className = 'justify-content-end d-flex message-container';
    div_autor.classList.add('justify-content-end', 'd-flex');

    autor.textContent = mensaje['agente'];

  } else {
    div_mensaje_clase.className = 'message-receive';
    div_container.className = 'col-sm-11';
    row_container.className = 'row message-container';

    autor.textContent = mensaje['autor'];
  }

  if (mensaje['caption']) {
    if (mensaje['tipo'] == 'image') {
      const div_caption = document.createElement('div');
      const span_caption = document.createElement('span');
      span_caption.classList.add('message-caption');
      span_caption.textContent = mensaje['caption'];

      div_caption.appendChild(span_caption);
      div_mensaje_tipo.appendChild(div_caption);
    }
  }

  div_mensaje_tipo.appendChild(div_fecha_mensaje);

  return row_container;
};

function convertir_enviar_imagen_producto(url, producto, callback) {
  var xhr = new XMLHttpRequest();
  xhr.onload = function () {
    var reader = new FileReader();
    reader.onloadend = function () {
      callback(producto, reader.result);
    }
    reader.readAsDataURL(xhr.response);
  };
  xhr.open('GET', url);
  xhr.responseType = 'blob';
  xhr.send();
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


// CONTROL DE NOTIFICACIONES PARA EL NAVEGADOR

console.log(Notification.permission);
if (Notification.permission === "granted") {
  // alert("we have permission");
  console.log("Notificaciones permitidas");
} else if (Notification.permission !== "denied") {
  Notification.requestPermission().then(permission => {
      console.log(permission);
  });
}

// FUNCION PARA MOSTRAR NOTIFICACIONES
function showNotification(mensaje='') {
  let titulo = "Nuevo mensaje TrueContact";
  // let mensaje = "cliente, body message"
  const notification = new Notification(titulo, {
     body: mensaje,
  })
  document.getElementById('mensaje_tctag').play();
}

function btn_mostrar_modal_whatsapp_manual() {
    $('#modal-whatsapp-campana-manual').modal('hide');
    var nombre_campana = $("#select-campana-manual").val();
    var id_dialogo = document.getElementById('id_chatapi').value
    console.log("ID DE CAMPANA", nombre_campana);
    // $('#id_calificacion_manual').empty();
    // $('#id_calificacion_manual').append("<option value=''>Seleccione</option>").trigger("chosen:updated");

    //CARGO LOS MENSAJES DEL DIALOGO
    let string_url = window.location.origin + Urls.tc_dialogo_mensajes(id_dialogo);
    let url = new URL(string_url),
      params = { 'estado': 'todos', 'nombre_campana': nombre_campana, 'nuevo_chat': 'SI' };
    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
    fetch(url, {
      method: 'get',
    })
      .then(function (res) {
        return res.json();
      })
      .then(function (respuesta) {
        let puede_gestionar = respuesta['puede_gestionar'];
        let dialogo_info = respuesta['dialogo'];
        if (puede_gestionar) {
          if (dialogo_info['pk_gestion'] == '' ) {   //SI EL PK NO EXISTE ES UNA GESTION SALIENTE NUEVA
            console.log(">>>> cargar_mensajes_dialogo >> Y CREO LA GESTION nUEVA :", dialogo_info);
            var origen_gestion = JSON.parse('{"origen": "None", "pk": ""}');

            const call_data_new = {
              'medio': dialogo_info['medio'],
              'id_contacto': dialogo_info['id_contacto'],
              'telefono': dialogo_info['numero'].slice(-10),
              'call_type': '4',
              'chat_id': dialogo_info['id'],
              'mensaje_inicial': dialogo_info['mensaje_inicial'],
              'id_campana': dialogo_info['id_campana'],
              'modelo_gestionar': origen_gestion
            };
            agregar_gestion_whatsapp(call_data_new);
          }
        }
        else{
          console.log("no puede gestionar");
        }
      })
  }

  function dialogo_pertece_a_campana (id_campana_msj){
    // TOMO DEL TEMPLATE LOS IDS Y LOS DEJO EN LISTA PARA QUE EL CHATAIPjS  SEPA QUE PINTA Y QUE NO
    var nombre_campana = document.getElementById('nombre_campana').value
    if (nombre_campana == id_campana_msj){
      var existe = true
    }else{

      var existe = id_campanas_agente.includes(String(id_campana_msj));
    }
    return existe;
  }
