/*
*******************************************************************
* Elementos
*******************************************************************
*/

var id_texto = document.getElementById('id_texto');

var c_nombre = document.getElementById('c_nombre');
var c_telefono = document.getElementById('c_telefono');
var c_email = document.getElementById('c_email');
var a_nombre_completo = document.getElementById('a_nombre_completo');
var a_nombre_corto = document.getElementById('a_nombre_corto');
var a_correo = document.getElementById('a_correo');

/*
*******************************************************************
* Clases
*******************************************************************
*/

c_nombre.addEventListener('click', function(){
    value_masivo = id_texto.value
    id_texto.value = value_masivo + c_nombre.innerText
})

c_telefono.addEventListener('click', function(){
    value_masivo = id_texto.value
    id_texto.value = value_masivo + c_telefono.innerText
})

c_email.addEventListener('click', function(){
    value_masivo = id_texto.value
    id_texto.value = value_masivo + c_email.innerText
})

a_nombre_completo.addEventListener('click', function(){
    value_masivo = id_texto.value
    id_texto.value = value_masivo + a_nombre_completo.innerText
})

a_nombre_corto.addEventListener('click', function(){
    value_masivo = id_texto.value
    id_texto.value = value_masivo + a_nombre_corto.innerText
})

a_correo.addEventListener('click', function(){
    value_masivo = id_texto.value
    id_texto.value = value_masivo + a_correo.innerText
})
